import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RoomResponse} from '../../classes/rooms/room-response';

@Component({
  selector: 'app-checkin-dialog',
  templateUrl: './checkin-dialog.component.html',
  styleUrls: ['./checkin-dialog.component.scss'],
})
export class CheckinDialogComponent implements OnInit {
  checkinTime: string = null;
  now: string;

  constructor(
    public dialogRef: MatDialogRef<CheckinDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public room: RoomResponse) {
  }

  ngOnInit(): void {
    this.now = (new Date()).getHours() + ':' + (new Date()).getMinutes();
    this.checkinTime = this.now;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCheckinClick(): void {
    if (this.checkinTime !== this.now) {
      this.dialogRef.close(this.checkinTime);
    } else {
      this.dialogRef.close('now');
    }
  }
}
