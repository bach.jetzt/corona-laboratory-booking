import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RoomResponse} from '../../classes/rooms/room-response';
import {BookingsService} from '../../services/bookings/bookings.service';
import {Filter} from '../../services/filter/filter';
import {BookingFilter} from '../../services/bookings/booking-filter';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {map, switchMap, tap} from 'rxjs/operators';
import {RelationalOperator} from '../../services/filter/relational-operator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {isToday} from '../../utils/is-today';
import {BookingResponse} from '../../classes/bookings/booking-response';

@Component({
  selector: 'app-checkin-dialog',
  templateUrl: './checkout-dialog.component.html',
  styleUrls: ['./checkout-dialog.component.scss'],
})
export class CheckoutDialogComponent implements OnInit {
  checkoutTime: string = null;
  minCheckoutTime = '00:00';
  now: string;
  inputInvalid = false;
  private correspondingBooking: BookingResponse;

  constructor(
    public dialogRef: MatDialogRef<CheckoutDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public room: RoomResponse,
    private bookingsService: BookingsService,
    private authService: AuthenticationService,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    const nowDate = new Date();
    this.now = nowDate.getHours() + ':' + nowDate.getMinutes();
    this.checkoutTime = this.now;

    this.authService.authenticatedUser$.pipe(
      map(u => u.id),
      switchMap(uid => {
        const filter = new Filter(BookingFilter);
        filter.setFilter('roomId', RelationalOperator.EQUALS, this.room.id);
        filter.setFilter('userId', RelationalOperator.EQUALS, uid);
        filter.setFilter('checkout', RelationalOperator.IS_NULL, 'true');
        return this.bookingsService.getFilteredBookings(filter);
      }),
      map(res => {
        return res.results.length ? res.results[0] : null
      }),
      tap(booking => {
        this.correspondingBooking = booking;
        const checkin = new Date(booking.checkin);
        if (isToday(checkin)) {
          this.minCheckoutTime = checkin.getHours() + ':' + checkin.getMinutes();
        }
      }),
    ).subscribe();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCheckoutClick(): void {
    if (!this.isValidEndTime()) {
      this.inputInvalid = false;
      return;
    }

    let checkoutTime: Date = null;
    if (this.checkoutTime !== this.now) {
      checkoutTime = new Date();
      checkoutTime.setHours(parseInt(this.checkoutTime.split(':')[0]));
      checkoutTime.setMinutes(parseInt(this.checkoutTime.split(':')[1]));
      checkoutTime.setSeconds(0);
    }
    this.bookingsService.checkout(this.room, checkoutTime).subscribe(() => {
      this.dialogRef.close(true);
    });
  }

  isValidEndTime(): boolean {
    const validInput = RegExp('^(?:2[0-3]|[01][0-9]):[0-5]?[0-9]$');
    if (!validInput.test(this.checkoutTime)) {
      return false;
    }

    const currentCheckoutTime = new Date();
    currentCheckoutTime.setHours(parseInt(this.checkoutTime.split(':')[0]));
    currentCheckoutTime.setMinutes(parseInt(this.checkoutTime.split(':')[1]));
    currentCheckoutTime.setSeconds(new Date(this.correspondingBooking.checkin).getSeconds());
    const now = new Date();
    now.setHours(parseInt(this.now.split(':')[0]));
    now.setMinutes(parseInt(this.now.split(':')[1]));
    now.setSeconds(59);
    return now >= currentCheckoutTime;
  }

  detectErrors() {
    this.inputInvalid = !this.isValidEndTime();
  }
}
