export interface UpdateTeam {
  name: string;
  forcedCheckoutTime: Date;
}
