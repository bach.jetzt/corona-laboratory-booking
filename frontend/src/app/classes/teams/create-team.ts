export interface CreateTeam {
  name: string;
  forcedCheckoutTime: Date;
}
