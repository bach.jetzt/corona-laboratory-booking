import {TeamResponse} from './team-response';
import {TeamPermission} from './team-permission';
import {DateTimeString} from '../date-time-string';
import {TeamMember} from './team-member';

export interface MyTeamResponse extends TeamResponse {
  permission: TeamPermission;
  forcedCheckoutTime: DateTimeString | null;
  members: TeamMember[];
}
