import {UserResponse} from '../auth/user-response';
import {TeamPermission} from './team-permission';

export interface TeamMember {
  user: UserResponse;
  permission: TeamPermission;
}
