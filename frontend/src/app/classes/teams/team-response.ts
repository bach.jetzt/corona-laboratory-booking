export interface TeamResponse {
  id: string;
  name: string;
  invitationCode: string;
  roomsCount: number;
  membersCount: number;
}
