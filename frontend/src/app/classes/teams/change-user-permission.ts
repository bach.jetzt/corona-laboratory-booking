import {uuid} from '../uuid';
import {TeamPermission} from './team-permission';

export interface ChangeUserPermission {
  userId: uuid;
  newPermission: TeamPermission;
}
