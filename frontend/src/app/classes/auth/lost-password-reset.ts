export interface LostPasswordReset {
  resetToken: string;
  password: string;
}
