import {MyTeamResponse} from "../teams/my-team-response";
import {RoomResponse} from '../rooms/room-response';

export class MyUserResponse {
  id: string;
  email: string;
  name: string;
  teams: MyTeamResponse[];
  roles: string[];
  favoriteRooms: RoomResponse[];
}
