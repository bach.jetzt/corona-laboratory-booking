import {uuid} from "../uuid";
import {DateTimeString} from "../date-time-string";

export interface Checkin {
  roomId: uuid;
  time: DateTimeString | null;
}
