import {uuid} from "../uuid";
import {RoomResponse} from "../rooms/room-response";
import {DateTimeString} from "../date-time-string";

export interface BookingResponse {
  id: uuid;
  room: RoomResponse;
  userId: uuid;
  checkin: DateTimeString;
  checkout: DateTimeString | null;
}
