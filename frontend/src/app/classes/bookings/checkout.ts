import {uuid} from "../uuid";
import {DateTimeString} from "../date-time-string";

export interface Checkout {
  roomId: uuid;
  time: DateTimeString | null;
}
