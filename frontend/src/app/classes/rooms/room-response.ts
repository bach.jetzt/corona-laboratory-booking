import {uuid} from "../uuid";
import {TeamResponse} from "../teams/team-response";
import {UserResponse} from "../auth/user-response";

export interface RoomResponse {
  id: uuid;
  number: string;
  floorName: string | null;
  building: string;
  allowedVisitors: number;
  currentVisitors: UserResponse[];
  team: TeamResponse;
  isFavorite: boolean;
}
