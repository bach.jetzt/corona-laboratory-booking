import {uuid} from "../uuid";

export interface UpdateRoom {
  number: string;
  floorName: string | null;
  building: string;
  allowedVisitors: number;
  teamId: uuid;
}
