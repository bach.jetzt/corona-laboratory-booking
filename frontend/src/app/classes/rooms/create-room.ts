import {uuid} from "../uuid";

export interface CreateRoom {
  number: string;
  floorName: string | null;
  building: string;
  allowedVisitors: number;
  teamId: uuid;
}
