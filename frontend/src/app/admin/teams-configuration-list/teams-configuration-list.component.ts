import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {faEdit} from '@fortawesome/free-regular-svg-icons';
import {PagedDataSource} from '../../services/paged-data-source';
import {RelationalOperator} from '../../services/filter/relational-operator';
import {merge, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {SortDirection} from '../../services/filter/sort-direction';
import {debounceTime, map, tap} from 'rxjs/operators';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {TeamFilter} from '../../services/teams/team-filter';
import {TeamsService} from '../../services/teams/teams.service';

@Component({
  selector: 'app-rooms-configuration-list',
  templateUrl: './teams-configuration-list.component.html',
  styleUrls: ['./teams-configuration-list.component.scss'],
})
export class TeamsConfigurationListComponent implements OnInit, AfterViewInit {
  faPlus = faPlus;
  faEdit = faEdit;

  columnsToDisplay = ['name', 'roomsCount', 'membersCount', 'actions'];
  filterRow = ['name-filter', 'roomsCount-filter', 'membersCount-filter', 'actions-filter'];
  dataSource: PagedDataSource<MyTeamResponse, TeamFilter>;

  allFilters$: Observable<[keyof TeamFilter, RelationalOperator, string | boolean]>;

  nameFilterValue = new FormControl('');
  nameFilterValue$: Observable<['name', RelationalOperator, string]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private teamsService: TeamsService,
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new PagedDataSource(this.teamsService.fetch, TeamFilter);
    // this.dataSource.registerRoute();
    this.dataSource.filter.setSort('name', SortDirection.ASC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.nameFilterValue$ = this.nameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['name', RelationalOperator.LIKE, v]),
    );

    this.allFilters$ = merge(this.nameFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof TeamFilter, RelationalOperator, string] | Sort | PageEvent) => this.loadTeamPage(change)),
      )
      .subscribe();
  }

  loadTeamPage<K extends keyof TeamFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setOffset(this.paginator.pageIndex * this.paginator.pageSize);
    this.dataSource.filter.setLimit(this.paginator.pageSize);
    this.dataSource.loadData();
  }

}
