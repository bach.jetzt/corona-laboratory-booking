import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsConfigurationListComponent } from './teams-configuration-list.component';

describe('TeamsConfigurationListComponent', () => {
  let component: TeamsConfigurationListComponent;
  let fixture: ComponentFixture<TeamsConfigurationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsConfigurationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsConfigurationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
