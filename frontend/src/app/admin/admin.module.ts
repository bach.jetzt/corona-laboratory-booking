import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TeamsCreateComponent} from './teams-create/teams-create.component';
import {TeamsConfigurationListComponent} from './teams-configuration-list/teams-configuration-list.component';
import {AdminRoutingModule} from './admin-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';


@NgModule({
  declarations: [TeamsCreateComponent, TeamsConfigurationListComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
  ],
})
export class AdminModule {
}
