import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TeamsCreateComponent} from './teams-create/teams-create.component';
import {TeamsConfigurationListComponent} from './teams-configuration-list/teams-configuration-list.component';

const routes: Routes = [
  {path: '', redirectTo: 'teams'},
  {path: 'teams', component: TeamsConfigurationListComponent},
  {path: 'add-team', component: TeamsCreateComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
