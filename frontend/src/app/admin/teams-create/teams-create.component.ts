import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TeamsService} from '../../services/teams/teams.service';
import {CreateTeam} from '../../classes/teams/create-team';

@Component({
  selector: 'app-teams-create',
  templateUrl: './teams-create.component.html',
  styleUrls: ['./teams-create.component.scss'],
})
export class TeamsCreateComponent implements OnInit {

  teamCreateForm = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private teamsService: TeamsService,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const form = this.teamCreateForm.getRawValue();
    const create: CreateTeam = {
      name: form.name,
      forcedCheckoutTime: null,
    };
    this.teamsService.createTeam(create).subscribe(() => {
      this._snackBar.open('Created team "' + create.name + '" successful!', null, {
        duration: 3000,
      });
      this.router.navigate(['/admin', 'teams']);
    });
  }
}
