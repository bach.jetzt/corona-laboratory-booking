import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LostPasswordReset} from '../../classes/auth/lost-password-reset';
import {MatSnackBar} from '@angular/material/snack-bar';
import {of} from 'rxjs';
import {delay, tap} from 'rxjs/operators';

@Component({
  selector: 'app-set-new-password',
  templateUrl: './set-new-password.component.html',
  styleUrls: ['./set-new-password.component.scss'],
})
export class SetNewPasswordComponent implements OnInit {
  passwordResetForm = this.fb.group({
    password: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
    passwordRepeat: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
  }, {validator: this.checkPasswordMatch});

  constructor(
    private fb: FormBuilder,
    private auth: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const resetPw: LostPasswordReset = {
      resetToken: this.route.snapshot.paramMap.get('token'),
      password: this.passwordResetForm.get('password').value,
    };
    this.auth.resetPassword(resetPw).subscribe(() => {
      this._snackBar.open('Password reset successfull.', '', {
        duration: 4000,
        panelClass: ['bg-success', 'text-dark'],
      });
      of(null).pipe(
        delay(3500),
        tap(() => {
          this.router.navigate(['/']);
        }),
      ).subscribe();
    });
  }

  checkPasswordMatch(form: FormGroup) {
    const pass = form.get('password').value;
    const confirmPass = form.get('passwordRepeat').value;

    return pass === confirmPass ? null : {notSamePassword: true};
  }
}
