import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LoginRequest} from '../../classes/auth/login-request';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm = this.fb.group({
    username: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required])],
    rememberMe: [true, Validators.compose([Validators.required])],
  });

  destroy$ = new Subject();

  constructor(private fb: FormBuilder, private auth: AuthenticationService, private router: Router) {
  }

  ngOnInit(): void {
    this.auth.authenticatedUser$.pipe(
      takeUntil(this.destroy$),
      tap(u => {
        if (u !== null && this.router.url.startsWith('/auth')) {
          if (this.auth.previousPageBeforeNotLoggedInRedirect !== null) {
            const previous = this.auth.previousPageBeforeNotLoggedInRedirect;
            this.auth.previousPageBeforeNotLoggedInRedirect = null;
            this.router.navigateByUrl(previous);
          } else {
            this.router.navigate(['']);
          }
        }
      }),
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmit() {
    let loginRequest: LoginRequest = this.loginForm.value;
    this.auth.login(loginRequest).subscribe(() => {
      if (this.auth.previousPageBeforeNotLoggedInRedirect !== null) {
        const previous = this.auth.previousPageBeforeNotLoggedInRedirect;
        this.auth.previousPageBeforeNotLoggedInRedirect = null;
        this.router.navigateByUrl(previous);
      } else {
        this.router.navigate(['']);
      }
    }, (err) => {
      this.loginForm.reset();
    });
  }
}
