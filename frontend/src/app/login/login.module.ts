import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SetNewPasswordComponent } from './set-new-password/set-new-password.component';


@NgModule({
  declarations: [LoginComponent, RegisterComponent, ResetPasswordComponent, SetNewPasswordComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
  ],
})
export class LoginModule {
}
