import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.min(5)])],
    passwordRepeat: ['', Validators.compose([Validators.required, Validators.min(5)])],
  }, {validator: this.checkPasswordMatch});

  constructor(private fb: FormBuilder, private auth: AuthenticationService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const registerRequest = this.registerForm.value;
    this.auth.register(registerRequest).subscribe(() => {
      this.router.navigate(['']);
    }, (err) => {
      this.registerForm.setErrors({'error': err.error.message});
    });
  }

  checkPasswordMatch(form: FormGroup) {
    const pass = form.get('password').value;
    const confirmPass = form.get('passwordRepeat').value;

    return pass === confirmPass ? null : {notSamePassword: true};
  }
}
