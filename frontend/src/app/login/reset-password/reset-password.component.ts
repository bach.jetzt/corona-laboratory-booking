import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {LostPassword} from '../../classes/auth/lost-password';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  passwordLostForm = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
  });
  success = false;

  constructor(
    private fb: FormBuilder,
    private auth: AuthenticationService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const lostPw: LostPassword = {
      username: this.passwordLostForm.get('email').value,
    };
    this.auth.lostPassword(lostPw).subscribe(() => {
      this.success = true;
    });
  }
}
