import {AbstractFilter} from '../filter/abstract-filter';
import {RelationalOperator} from '../filter/relational-operator';

export class RoomFilter extends AbstractFilter {
  public id: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public search: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public building: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public floorName: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public number: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public allowedVisitors: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public order: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
}
