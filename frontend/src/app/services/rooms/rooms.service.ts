import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, tap, toArray} from 'rxjs/operators';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../filter/filter';
import {PagedResult} from '../paged-result';
import {RoomFilter} from './room-filter';
import {RoomResponse} from '../../classes/rooms/room-response';
import {uuid} from '../../classes/uuid';
import {CreateRoom} from '../../classes/rooms/create-room';
import {SortDirection} from '../filter/sort-direction';
import {UpdateRoom} from '../../classes/rooms/update-room';
import {RelationalOperator} from '../filter/relational-operator';
import {MyUserResponse} from '../../classes/auth/my-user-response';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class RoomsService extends AbstractApiService<RoomResponse> {
  baseUrl = '/api/v1/rooms';

  constructor(public http: HttpClient, private _snackBar: MatSnackBar, private authService: AuthenticationService) {
    super();
  }

  getFilteredRooms(filter: Filter<RoomFilter>): Observable<PagedResult<RoomResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<RoomFilter>) => this.getFilteredRooms(filter);

  getRoomById(id: uuid): Observable<RoomResponse | null> {
    const filter = new Filter(RoomFilter, 0, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.totalCount !== 1) {
          throw new Error('Room with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  createRoom(createMessage: CreateRoom): Observable<RoomResponse> {
    return this.postGeneric<CreateRoom, RoomResponse>(this.baseUrl, createMessage);
  }

  updateRoom(id: uuid, updateMessage: UpdateRoom): Observable<RoomResponse> {
    return this.putGeneric<UpdateRoom, RoomResponse>(this.baseUrl + '/' + id, updateMessage);
  }

  delete(room: RoomResponse) {
    return this.deleteGeneric<null>(this.baseUrl + '/' + room.id);
  }

  getAllRoomsFiltered(filter: Filter<RoomFilter>): Observable<RoomResponse[]> {
    filter.setOffset(0);
    if (filter.getSort('order') === SortDirection.NONE) {
      filter.setSort('order', SortDirection.ASC);
    }
    return this.getFilteredRooms(filter).pipe(
      expand(page => {
        if (page.offset + page.count < page.totalCount) {
          filter.setOffset(page.offset + page.limit);
          return this.getFilteredRooms(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }

  markAsFavorite(room: RoomResponse): Observable<MyUserResponse> {
    return this.postGeneric<null, MyUserResponse>(this.baseUrl + '/favorites/' + room.id, null).pipe(
      tap(() => {
        this.authService.reloadUser().subscribe();
        this._snackBar.open('Marked room "' + room.number + '" as favorite!', null, {
          duration: 3000,
        });
      }),
    );
  }

  removeFromFavorites(room: RoomResponse): Observable<MyUserResponse> {
    return this.deleteGeneric<MyUserResponse>(this.baseUrl + '/favorites/' + room.id).pipe(tap(() => {
        this.authService.reloadUser().subscribe();
        this._snackBar.open('Removed room "' + room.number + '" from favorites!', null, {
          duration: 3000,
        });
      }),
    );
  }
}
