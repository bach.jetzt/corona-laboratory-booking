export class Pagination {
  public offset = 0;
  public limit = 20;

  constructor(offset: number, limit: number) {
    this.offset = offset;
    this.limit = limit;
  }
}
