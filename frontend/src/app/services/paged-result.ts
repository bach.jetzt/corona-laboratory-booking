export class PagedResult<T> {
    totalCount: number;
    offset: number;
    limit: number;
    count: number;
    results: T[];
}
