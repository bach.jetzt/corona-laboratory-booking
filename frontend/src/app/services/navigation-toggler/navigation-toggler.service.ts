import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavigationTogglerService {
  private static localStorageClbToggleKey = 'clb-nav-toggled';

  private isToggled = new ReplaySubject<boolean>(1);
  isToggled$ = this.isToggled.asObservable();

  constructor() {
    const toggleState = localStorage.getItem(NavigationTogglerService.localStorageClbToggleKey);
    if (toggleState !== null) {
      this.isToggled.next(toggleState.toLowerCase() === 'true');
    }
  }

  setToggleState(state: boolean) {
    localStorage.setItem(NavigationTogglerService.localStorageClbToggleKey, state ? 'true' : 'false');
    this.isToggled.next(state);
  }
}
