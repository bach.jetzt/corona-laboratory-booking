import { TestBed } from '@angular/core/testing';

import { NavigationTogglerService } from './navigation-toggler.service';

describe('NavigationTogglerService', () => {
  let service: NavigationTogglerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavigationTogglerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
