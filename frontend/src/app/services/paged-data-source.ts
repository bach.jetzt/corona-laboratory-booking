import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {PagedResult} from './paged-result';
import {AbstractFilter} from './filter/abstract-filter';
import {Filter} from './filter/filter';

export class PagedDataSource<T, R extends AbstractFilter> implements DataSource<T> {

  fetch: (filter: Filter<R>) => Observable<PagedResult<T>>;
  filter: Filter<R>;
  totalCount = 1;
  offset = 0;
  limit = 20;

  private dataSubject = new BehaviorSubject<T[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  constructor(callable: (filter: Filter<R>) => Observable<PagedResult<T>>, filterClass: new() => R) {
    this.filter = new Filter(filterClass);
    this.fetch = callable;
  }

  connect(collectionViewer: CollectionViewer): Observable<T[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
    this.loadingSubject.complete();
  }

  loadData() {
    this.loadingSubject.next(true);

    this.fetch(this.filter).pipe(
      catchError(() => of(new PagedResult<T>())),
      finalize(() => this.loadingSubject.next(false)),
    ).subscribe((pr: PagedResult<T>) => {
      this.offset = pr.offset === undefined ? 0 : pr.offset;
      this.limit = pr.limit === undefined ? 8 : pr.limit;
      this.totalCount = pr.totalCount === undefined ? 0 : pr.totalCount;
      this.dataSubject.next(pr.results);
    });
  }
}
