import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {LoginRequest} from '../../classes/auth/login-request';
import {HttpClient} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {CreateUser} from '../../classes/auth/create-user';
import {MyUserResponse} from '../../classes/auth/my-user-response';
import {LostPassword} from '../../classes/auth/lost-password';
import {LostPasswordReset} from '../../classes/auth/lost-password-reset';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private authenticatedUser = new ReplaySubject<MyUserResponse | null>(1);

  previousPageBeforeNotLoggedInRedirect = null;

  authenticatedUser$: Observable<MyUserResponse | null> = this.authenticatedUser.asObservable().pipe(
    map(u => {
      if (u === null) {
        return u;
      }
      u.favoriteRooms.forEach(r => r.isFavorite = true)
      return u;
    }),
  );
  isAdmin$: Observable<boolean> = this.authenticatedUser$.pipe(map(u => {
    return u !== null && u.roles.indexOf('ROLE_ADMIN') > -1;
  }));
  isMemberOfAnyTeam$: Observable<boolean> = this.authenticatedUser$.pipe(map(u => {
    return u !== null && u.teams.length > 0;
  }));
  isOwnerOfAnyTeam$: Observable<boolean> = this.authenticatedUser$.pipe(map(u => {
    return u !== null && u.teams.filter(team => team.permission === 'OWNER').length > 0;
  }));

  constructor(private http: HttpClient) {
    this.reloadUser().pipe(
      delay(100),
    ).subscribe();
  }

  reloadUser() {
    return this.http.get<MyUserResponse>('api/v1/me').pipe(tap(
      resp => this.authenticatedUser.next(resp),
      ),
      catchError(err => {
        this.authenticatedUser.next(null);
        throw err;
      }),
    );
  }

  login(loginRequest: LoginRequest): Observable<MyUserResponse> {
    return this.http.post<MyUserResponse>('api/v1/auth/login', loginRequest).pipe(tap(
      resp => this.authenticatedUser.next(resp),
      ),
      catchError(err => {
        this.authenticatedUser.next(null);
        throw err;
      }),
    );
  }

  register(createUser: CreateUser): Observable<MyUserResponse> {
    return this.http.post<MyUserResponse>('api/v1/auth/register', createUser).pipe(
      catchError(err => {
        throw err;
      }),
    );
  }

  logout() {
    return this.http.get('api/v1/auth/logout').pipe(tap(
      resp => this.authenticatedUser.next(null),
      ),
      catchError(err => {
        this.authenticatedUser.next(null);
        throw err;
      }),
    );
  }

  lostPassword(lostPw: LostPassword) {
    return this.http.post('api/v1/auth/lost-password', lostPw);
  }

  resetPassword(resetPw: LostPasswordReset) {
    return this.http.post('api/v1/auth/lost-password-reset', resetPw);
  }
}
