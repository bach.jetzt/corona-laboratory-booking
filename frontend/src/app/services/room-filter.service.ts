import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RoomFilterService {
  private _reset = new Subject();

  reset$ = this._reset.asObservable();

  constructor() {
  }

  reset() {
    this._reset.next();
  }
}
