import {Injectable} from '@angular/core';
import {EMPTY, Observable} from 'rxjs';
import {TeamResponse} from '../../classes/teams/team-response';
import {HttpClient} from '@angular/common/http';
import {uuid} from '../../classes/uuid';
import {MyUserResponse} from '../../classes/auth/my-user-response';
import {AuthenticationService} from '../authentication/authentication.service';
import {concatMap, expand, map, tap, toArray} from 'rxjs/operators';
import {AbstractApiService} from '../abstract-api.service';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {Filter} from '../filter/filter';
import {PagedResult} from '../paged-result';
import {RelationalOperator} from '../filter/relational-operator';
import {TeamFilter} from './team-filter';
import {SortDirection} from '../filter/sort-direction';
import {CreateTeam} from '../../classes/teams/create-team';
import {UpdateTeam} from '../../classes/teams/update-team';
import {UserResponse} from '../../classes/auth/user-response';
import {TeamPermission} from '../../classes/teams/team-permission';
import {ChangeUserPermission} from '../../classes/teams/change-user-permission';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class TeamsService extends AbstractApiService<MyTeamResponse> {
  baseUrl = '/api/v1/teams';

  constructor(
    public http: HttpClient,
    private auth: AuthenticationService,
    private _snackBar: MatSnackBar,
  ) {
    super();
  }

  getFilteredTeams(filter: Filter<TeamFilter>): Observable<PagedResult<MyTeamResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<TeamFilter>) => this.getFilteredTeams(filter);

  getTeamById(id: uuid): Observable<MyTeamResponse | null> {
    const filter = new Filter(TeamFilter, 0, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        // TODO revert to: if (r.totalCount !== 1) {
        if (r.results.length !== 1) {
          throw new Error('Team with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  getAllTeamsFiltered(filter: Filter<TeamFilter>): Observable<MyTeamResponse[]> {
    filter.setOffset(0);
    if (filter.getSort('order') === SortDirection.NONE) {
      filter.setSort('order', SortDirection.ASC);
    }
    return this.getFilteredTeams(filter).pipe(
      expand(page => {
        if (page.offset + page.count < page.totalCount) {
          filter.setOffset(page.offset + page.limit);
          return this.getFilteredTeams(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }

  getTeamByInvitationCode(invitationCode: uuid): Observable<TeamResponse> {
    return this.http.get<TeamResponse>(this.baseUrl + '/' + invitationCode);
  }

  joinTeam(id: uuid, invitationCode: string) {
    return this.http.put<MyUserResponse>(this.baseUrl + '/' + invitationCode + '/join', {}).pipe(
      tap(() => this.auth.reloadUser().subscribe()),
    );
  }

  createTeam(createMessage: CreateTeam) {
    return this.postGeneric<CreateTeam, TeamResponse>(this.baseUrl, createMessage);
  }

  updateTeam(teamId: uuid, updateMessage: UpdateTeam) {
    return this.putGeneric<UpdateTeam, TeamResponse>(this.baseUrl + '/' + teamId, updateMessage);
  }

  regenerateToken(team: MyTeamResponse) {
    return this.putGeneric<UpdateTeam, TeamResponse>(this.baseUrl + '/' + team.id + '/renew-token', null).pipe(
      tap(() => {
        this.auth.reloadUser().subscribe();
      }),
    );
  }

  removeUserFromTeam(team: TeamResponse, user: UserResponse) {
    return this.http.put(this.baseUrl + '/' + team.id + '/remove-user/' + user.id, null).pipe(
      tap(() => {
        this.auth.reloadUser().subscribe();
      }),
    );
  }

  changeUserPermission(team: TeamResponse, user: UserResponse, permission: TeamPermission) {
    const changeUserRequest: ChangeUserPermission = {
      userId: user.id,
      newPermission: permission,
    };
    return this.putGeneric<ChangeUserPermission, MyTeamResponse>(this.baseUrl + '/' + team.id + '/change-user-permission', changeUserRequest).pipe(tap(() => {
      this._snackBar.open('Changed permissions of user "' + user.name + '".', null, {
        duration: 3000,
      });
    }));
  }
}
