import {AbstractFilter} from '../filter/abstract-filter';
import {RelationalOperator} from '../filter/relational-operator';

export class TeamFilter extends AbstractFilter {
  public id: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public name: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public roomsCount: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public membersCount: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public order: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
}
