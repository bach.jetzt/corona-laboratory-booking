import {Injectable} from '@angular/core';
import {AbstractApiService} from '../abstract-api.service';
import {HttpClient} from '@angular/common/http';
import {Filter} from '../filter/filter';
import {EMPTY, Observable} from 'rxjs';
import {PagedResult} from '../paged-result';
import {BookingFilter} from './booking-filter';
import {BookingResponse} from '../../classes/bookings/booking-response';
import {uuid} from '../../classes/uuid';
import {Checkin} from '../../classes/bookings/checkin';
import {SortDirection} from '../filter/sort-direction';
import {concatMap, expand, map, tap, toArray} from 'rxjs/operators';
import {RelationalOperator} from '../filter/relational-operator';
import {Checkout} from '../../classes/bookings/checkout';
import {MatSnackBar} from '@angular/material/snack-bar';
import {RoomResponse} from '../../classes/rooms/room-response';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class BookingsService extends AbstractApiService<BookingResponse> {
  baseUrl = '/api/v1/bookings';

  constructor(
    public http: HttpClient,
    private _snackBar: MatSnackBar,
    private auth: AuthenticationService,
  ) {
    super();
  }

  getFilteredBookings(filter: Filter<BookingFilter>): Observable<PagedResult<BookingResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<BookingFilter>) => this.getFilteredBookings(filter);

  getBookingById(id: uuid): Observable<BookingResponse | null> {
    const filter = new Filter(BookingFilter, 0, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.totalCount !== 1) {
          throw new Error('Booking with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  getAllBookingsFiltered(filter: Filter<BookingFilter>): Observable<BookingResponse[]> {
    filter.setOffset(0);
    if (filter.getSort('order') === SortDirection.NONE) {
      filter.setSort('order', SortDirection.ASC);
    }
    return this.getFilteredBookings(filter).pipe(
      expand(page => {
        if (page.offset + page.count < page.totalCount) {
          filter.setOffset(page.offset + page.limit);
          return this.getFilteredBookings(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }

  checkin(room: RoomResponse, time: Date = null) {
    const cm: Checkin = {
      roomId: room.id,
      time: time !== null ? time.toISOString() : null,
    };
    return this.postGeneric<Checkin, BookingResponse>(this.baseUrl + '/checkin', cm).pipe(
      tap(() => {
        this._snackBar.open('Check in to room "' + room.number + '" successful!', null, {
          duration: 5000,
        });
        this.auth.reloadUser().subscribe();
      }),
    );
  }

  checkout(room: RoomResponse, time: Date = null) {
    const cm: Checkout = {
      roomId: room.id,
      time: time !== null ? time.toISOString() : null,
    };
    return this.postGeneric<Checkout, BookingResponse>(this.baseUrl + '/checkout', cm).pipe(
      tap(() => {
        this._snackBar.open('Check out from room "' + room.number + '" successful!', null, {
          duration: 5000,
        });
        this.auth.reloadUser().subscribe();
      }),
    );
  }
}
