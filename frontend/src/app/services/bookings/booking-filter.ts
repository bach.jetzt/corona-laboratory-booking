import {AbstractFilter} from '../filter/abstract-filter';
import {RelationalOperator} from '../filter/relational-operator';
import {uuid} from '../../classes/uuid';

export class BookingFilter extends AbstractFilter {
  public id: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public checkin: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public checkout: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public userId: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public roomId: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public order: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
}
