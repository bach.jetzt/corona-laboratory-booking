import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DialogLogoutComponent, MainPageComponent} from './pages/main-page/main-page.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {ClipboardModule} from 'ngx-clipboard';
import {RoomsListComponent} from './pages/rooms-list/rooms-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SafeUrlPipe} from './utils/safe-url.pipe';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {QRCodeModule} from 'angularx-qrcode';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {JoinTeamComponent} from './pages/join-team/join-team.component';
import {AuthInterceptor} from './login/auth.interceptor';
import {CheckinDialogComponent} from './dialogs/checkin-dialog/checkin-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {OpenBookingCardComponent} from './components/open-booking-card/open-booking-card.component';
import {RoomsConfigurationListComponent} from './pages/rooms-configuration-list/rooms-configuration-list.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {RoomCardComponent} from './components/room-card/room-card.component';
import {CheckoutDialogComponent} from './dialogs/checkout-dialog/checkout-dialog.component';
import {DeleteRoomConfirmationDialog, RoomsEditComponent} from './pages/rooms-edit/rooms-edit.component';
import {RoomsCreateComponent} from './pages/rooms-create/rooms-create.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {RegenerateTeamInvitationTokenDialog, TeamsListComponent} from './pages/teams-list/teams-list.component';
import {DialogInvitationLinkQrCode, TeamCardComponent} from './components/team-card/team-card.component';
import {DeleteTeamMemberConfirmationDialog, TeamsEditComponent} from './pages/teams-edit/teams-edit.component';
import {ReportsComponent} from './pages/reports/reports.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateAdapter, MatNativeDateModule} from '@angular/material/core';
import {StartMondayDateAdapter} from './utils/start-monday-date-adapter';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    DashboardComponent,
    RoomsListComponent,
    SafeUrlPipe,
    NotFoundPageComponent,
    DialogInvitationLinkQrCode,
    DialogLogoutComponent,
    JoinTeamComponent,
    CheckinDialogComponent,
    CheckoutDialogComponent,
    OpenBookingCardComponent,
    RoomsConfigurationListComponent,
    RoomCardComponent,
    RoomsEditComponent,
    RoomsCreateComponent,
    DeleteRoomConfirmationDialog,
    TeamsListComponent,
    TeamCardComponent,
    RegenerateTeamInvitationTokenDialog,
    TeamsEditComponent,
    DeleteTeamMemberConfirmationDialog,
    ReportsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ClipboardModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatDialogModule,
    ReactiveFormsModule,
    QRCodeModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    NgxMaterialTimepickerModule,
    FontAwesomeModule,
    MatSlideToggleModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {provide: DateAdapter, useClass: StartMondayDateAdapter},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
