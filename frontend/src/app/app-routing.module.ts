import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from './pages/main-page/main-page.component';
import {IsLoggedInGuard} from './guards/is-logged-in.guard';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {RoomsListComponent} from './pages/rooms-list/rooms-list.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';
import {JoinTeamComponent} from './pages/join-team/join-team.component';
import {RoomsConfigurationListComponent} from './pages/rooms-configuration-list/rooms-configuration-list.component';
import {RoomsEditComponent} from './pages/rooms-edit/rooms-edit.component';
import {RoomResolverService} from './guards/room-resolver.service';
import {RoomsCreateComponent} from './pages/rooms-create/rooms-create.component';
import {TeamResolverService} from './guards/team-resolver.service';
import {TeamsListComponent} from './pages/teams-list/teams-list.component';
import {IsAdminGuard} from './guards/is-admin.guard';
import {TeamsEditComponent} from './pages/teams-edit/teams-edit.component';
import {ReportsComponent} from './pages/reports/reports.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MainPageComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'rooms',
        component: RoomsListComponent,
      },
      {
        path: 'manage/rooms',
        component: RoomsConfigurationListComponent,
      },
      {
        path: 'manage/add-room',
        component: RoomsCreateComponent,
      },
      {
        path: 'manage/rooms/:roomId',
        component: RoomsEditComponent,
        resolve: {room: RoomResolverService},
      },
      {
        path: 'manage/teams',
        component: TeamsListComponent,
      },
      {
        path: 'manage/teams/:teamId',
        component: TeamsEditComponent,
        resolve: {team: TeamResolverService},
      },
      {
        path: 'manage/reports',
        component: ReportsComponent,
      },
      {
        path: 'join-team/:invitationCode',
        component: JoinTeamComponent,
      },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
        canActivate: [IsAdminGuard],
      },
    ],
  },
  {
    path: 'auth',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: '**',
    component: MainPageComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      {
        path: '',
        component: NotFoundPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
