import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Location} from '@angular/common';
import {TeamsService} from '../services/teams/teams.service';
import {TeamResponse} from '../classes/teams/team-response';

@Injectable({
  providedIn: 'root',
})
export class TeamResolverService implements Resolve<TeamResponse> {
  constructor(
    private teamsService: TeamsService,
    private _router: Router,
    private _location: Location,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TeamResponse> | Promise<TeamResponse> | TeamResponse {
    return this.teamsService.getTeamById(route.params.teamId).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this._router.navigate(['404']).then(() => {
            this._location.replaceState(state.url);
          })),
        );
      }),
    );
  }
}
