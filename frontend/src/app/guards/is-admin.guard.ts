import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../services/authentication/authentication.service';
import {map, take, tap} from 'rxjs/operators';
import {Location} from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class IsAdminGuard implements CanActivate {
  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private _location: Location,
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.auth.authenticatedUser$.pipe(
      take(1),
      map(u => u !== null && u.roles.filter(ur => ur === 'ROLE_ADMIN').length > 0),
      tap(success => {
        if (!success) {
          this.router.navigate(['404']).then(() => {
            this._location.replaceState(state.url);
          })
        }
      }),
    );
  }

}
