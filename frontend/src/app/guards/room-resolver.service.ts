import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {RoomResponse} from '../classes/rooms/room-response';
import {RoomsService} from '../services/rooms/rooms.service';
import {catchError, tap} from 'rxjs/operators';
import {Location} from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class RoomResolverService implements Resolve<RoomResponse> {
  constructor(
    private roomsService: RoomsService,
    private _router: Router,
    private _location: Location,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RoomResponse> | Promise<RoomResponse> | RoomResponse {
    return this.roomsService.getRoomById(route.params.roomId).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this._router.navigate(['404']).then(() => {
            this._location.replaceState(state.url);
          })),
        );
      }),
    );
  }
}
