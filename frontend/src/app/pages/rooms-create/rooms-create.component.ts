import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {RoomsService} from '../../services/rooms/rooms.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CreateRoom} from '../../classes/rooms/create-room';
import {TeamsService} from '../../services/teams/teams.service';
import {Filter} from '../../services/filter/filter';
import {TeamFilter} from '../../services/teams/team-filter';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-rooms-create',
  templateUrl: './rooms-create.component.html',
  styleUrls: ['./rooms-create.component.scss'],
})
export class RoomsCreateComponent implements OnInit {

  teams$: Observable<MyTeamResponse[]>;

  roomCreateForm = this.fb.group({
    teamId: ['', Validators.compose([Validators.required])],
    number: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    floorName: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    building: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    allowedVisitors: ['', Validators.compose([Validators.required, Validators.min(0)])],
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private roomsService: RoomsService,
    private _snackBar: MatSnackBar,
    private teamsService: TeamsService,
  ) {
  }

  ngOnInit() {
    const filter = new Filter(TeamFilter);
    this.teams$ = this.teamsService.getAllTeamsFiltered(filter);
  }

  onSubmit() {
    const form = this.roomCreateForm.getRawValue();
    const create: CreateRoom = {
      number: form.number,
      floorName: form.floorName,
      building: form.building,
      allowedVisitors: form.allowedVisitors,
      teamId: form.teamId,
    };
    this.roomsService.createRoom(create).subscribe(() => {
      this._snackBar.open('Created room "' + create.number + '" successful!', null, {
        duration: 3000,
      });
      this.router.navigate(['/manage', 'rooms']);
    });
  }
}
