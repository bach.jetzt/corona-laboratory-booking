import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {TeamsService} from '../../services/teams/teams.service';
import {UpdateTeam} from '../../classes/teams/update-team';
import {UserResponse} from '../../classes/auth/user-response';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {faLevelDownAlt, faLevelUpAlt, faUserTimes} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-teams-edit',
  templateUrl: './teams-edit.component.html',
  styleUrls: ['./teams-edit.component.scss'],
})
export class TeamsEditComponent implements OnInit {

  team: MyTeamResponse;

  teamEditForm = this.fb.group({
    id: [''],
    name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
    forcedCheckoutTime: [null, Validators.compose([Validators.pattern('^\\d{1,2}:\\d{1,2}$')])],
  });
  faUserTimes = faUserTimes;
  faLevelUpAlt = faLevelUpAlt;
  faLevelDownAlt = faLevelDownAlt;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private teamsService: TeamsService,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
    public auth: AuthenticationService,
  ) {
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: { team: MyTeamResponse }) => {
        this.team = data.team;
        this.teamEditForm.patchValue(this.team);
        if (this.team.forcedCheckoutTime !== null) {
          const checkoutTime = new Date(this.team.forcedCheckoutTime);
          this.teamEditForm.get('forcedCheckoutTime').patchValue(checkoutTime.getHours() + ':' + checkoutTime.getMinutes());
        }
      });
  }

  onSubmit() {
    const form = this.teamEditForm.getRawValue();
    let checkoutTime = null;
    if (form.forcedCheckoutTime && form.forcedCheckoutTime.split(':').length === 2) {
      const time = form.forcedCheckoutTime.split(':');
      checkoutTime = new Date();
      checkoutTime.setHours(time[0]);
      checkoutTime.setMinutes(time[1]);
      checkoutTime.setSeconds(0);
    }
    const update: UpdateTeam = {
      name: form.name,
      forcedCheckoutTime: checkoutTime,
    };
    this.teamsService.updateTeam(this.team.id, update).subscribe(() => {
      this._snackBar.open('Updated team "' + this.team.name + '" successful!', null, {
        duration: 3000,
      });
      this.router.navigate(['/manage', 'teams']);
    });
  }

  revokeMembership(user: UserResponse) {
    const dialogRef = this.dialog.open(DeleteTeamMemberConfirmationDialog, {
      maxWidth: '500px',
      width: '80%',
      data: user,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result === true) {
        this.teamsService.removeUserFromTeam(this.team, user).subscribe(() => {
          this._snackBar.open('Removed ' + user.name + ' from team "' + this.team.name + '"!', null, {
            duration: 5000,
          });
          this.router.navigate(['/manage', 'teams']);
        });
      }
    });
  }

  promoteUser(user: UserResponse) {
    this.teamsService.changeUserPermission(this.team, user, 'OWNER').subscribe(r => this.team = r);
  }

  downgradeUser(user: UserResponse) {
    this.teamsService.changeUserPermission(this.team, user, 'USER').subscribe(r => this.team = r);
  }
}

@Component({
  selector: 'delete-team-member-confirmation-dialog',
  template: `
    <h1 mat-dialog-title>Confirmation required
      <button class="fa-pull-right btn" aria-label="Close" [mat-dialog-close]="undefined"><span
        aria-hidden="true">x</span></button>
    </h1>
    <div mat-dialog-content>
      <p>This action leads to data loss. To prevent accidental actions we ask you to confirm your intention.</p>
      <p>Du you really want to remove <code>{{user.name}}</code> from your team?</p>
    </div>
    <div mat-dialog-actions>
      <button mat-button color="primary" [mat-dialog-close]="false">Cancel</button>
      <button mat-button color="warn" [mat-dialog-close]="true">Revoke Membership</button>
    </div>`,
})
export class DeleteTeamMemberConfirmationDialog {

  constructor(
    public dialogRef: MatDialogRef<DeleteTeamMemberConfirmationDialog>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public user: UserResponse,
  ) {
  }
}
