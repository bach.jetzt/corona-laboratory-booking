import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsConfigurationListComponent } from './rooms-configuration-list.component';

describe('RoomsConfigurationListComponent', () => {
  let component: RoomsConfigurationListComponent;
  let fixture: ComponentFixture<RoomsConfigurationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomsConfigurationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsConfigurationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
