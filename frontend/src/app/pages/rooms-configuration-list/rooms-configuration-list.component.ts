import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {faEdit} from '@fortawesome/free-regular-svg-icons';
import {PagedDataSource} from '../../services/paged-data-source';
import {RoomResponse} from '../../classes/rooms/room-response';
import {RoomFilter} from '../../services/rooms/room-filter';
import {RelationalOperator} from '../../services/filter/relational-operator';
import {merge, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {RoomsService} from '../../services/rooms/rooms.service';
import {SortDirection} from '../../services/filter/sort-direction';
import {debounceTime, map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-rooms-configuration-list',
  templateUrl: './rooms-configuration-list.component.html',
  styleUrls: ['./rooms-configuration-list.component.scss'],
})
export class RoomsConfigurationListComponent implements OnInit, AfterViewInit {
  faPlus = faPlus;
  faEdit = faEdit;

  columnsToDisplay = ['building', 'floorName', 'number', 'allowedVisitors', 'team', 'actions'];
  filterRow = ['building-filter', 'floorName-filter', 'number-filter', 'allowedVisitors-filter', 'team-filter', 'actions-filter'];
  dataSource: PagedDataSource<RoomResponse, RoomFilter>;

  allFilters$: Observable<[keyof RoomFilter, RelationalOperator, string | boolean]>;

  buildingFilterValue = new FormControl('');
  buildingFilterValue$: Observable<['building', RelationalOperator, string]>;
  floorFilterValue = new FormControl('');
  floorFilterValue$: Observable<['floorName', RelationalOperator, string]>;
  numberFilterValue = new FormControl('');
  numberFilterValue$: Observable<['number', RelationalOperator, string]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private roomsService: RoomsService,
  ) {
  }

  ngOnInit(): void {
    this.dataSource = new PagedDataSource(this.roomsService.fetch, RoomFilter);
    // this.dataSource.registerRoute();
    this.dataSource.filter.setSort('search', SortDirection.ASC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.numberFilterValue$ = this.numberFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['number', RelationalOperator.LIKE, v]),
    );
    this.floorFilterValue$ = this.floorFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['floorName', RelationalOperator.LIKE, v]),
    );
    this.buildingFilterValue$ = this.buildingFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['building', RelationalOperator.LIKE, v]),
    );

    this.allFilters$ = merge(this.numberFilterValue$, this.floorFilterValue$, this.buildingFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof RoomFilter, RelationalOperator, string] | Sort | PageEvent) => this.loadRoomPage(change)),
      )
      .subscribe();
  }

  loadRoomPage<K extends keyof RoomFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setOffset(this.paginator.pageIndex * this.paginator.pageSize);
    this.dataSource.filter.setLimit(this.paginator.pageSize);
    this.dataSource.loadData();
  }

}
