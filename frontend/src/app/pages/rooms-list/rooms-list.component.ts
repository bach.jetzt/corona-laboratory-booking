import {Component, OnInit} from '@angular/core';
import {RoomsService} from '../../services/rooms/rooms.service';
import {Filter} from '../../services/filter/filter';
import {RoomFilter} from '../../services/rooms/room-filter';
import {RoomResponse} from '../../classes/rooms/room-response';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, combineLatest, merge} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {CheckinDialogComponent} from '../../dialogs/checkin-dialog/checkin-dialog.component';
import {BookingsService} from '../../services/bookings/bookings.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {faCircleNotch} from '@fortawesome/free-solid-svg-icons';
import {RelationalOperator} from '../../services/filter/relational-operator';
import {debounceTime, first, map, skip, tap} from 'rxjs/operators';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {CheckoutDialogComponent} from '../../dialogs/checkout-dialog/checkout-dialog.component';
import {SortDirection} from '../../services/filter/sort-direction';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {MyUserResponse} from '../../classes/auth/my-user-response';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss'],
})
export class RoomsListComponent implements OnInit {

  filter = new Filter(RoomFilter);
  rooms: RoomResponse[];
  search: string;
  isLoading$ = new BehaviorSubject(true);
  faCircleNotch = faCircleNotch;
  filterOnlyFree = false;

  constructor(
    private authService: AuthenticationService,
    private roomsService: RoomsService,
    private bookings: BookingsService,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    if ((/true/i).test(localStorage.getItem('roomsFilterOnlyFree'))) {
      this.filterOnlyFree = true;
    }

    let initialSearch$ = this.route.queryParams.pipe(
      map((qp) => qp.search),
      first(),
    );
    let debouncedSearch$ = this.route.queryParams.pipe(
      map((qp) => qp.search),
      skip(1),
      debounceTime(400),
    );

    this.filter.setLimit(50);
    this.filter.setSort('search', SortDirection.ASC);

    merge(initialSearch$, debouncedSearch$).pipe(
      tap((search: string) => {
        this.search = search;
        if (this.search) {
          this.filter.setFilter('search', RelationalOperator.LIKE, this.search);
        } else {
          this.filter.clearFilter('search');
        }
        this.load();
      }),
    ).subscribe();
  }

  load() {
    this.isLoading$.next(true);
    combineLatest([this.roomsService.getAllRoomsFiltered(this.filter), this.authService.authenticatedUser$]).pipe(
      map((resp: [RoomResponse[], MyUserResponse | null]) => {
        if (this.filterOnlyFree === true) {
          resp[0] = resp[0].filter(rr => rr.currentVisitors.length < rr.allowedVisitors);
        }
        return resp;
      }),
      map((resp: [RoomResponse[], MyUserResponse | null]) => {
        resp[0].forEach(room => {
          room.isFavorite = resp[1].favoriteRooms.filter(fav => room.id === fav.id).length > 0;
        });
        return resp[0];
      }),
    ).subscribe(resp => {
      this.rooms = resp;
      this.isLoading$.next(false);
    });
  }

  handleUserFilterChange(ev: MatSlideToggleChange) {
    if (ev.source.name === 'onlyFree') {
      this.filterOnlyFree = ev.checked;
      localStorage.setItem('roomsFilterOnlyFree', String(ev.checked));
    }
    this.load();
  }
}
