import {Component, OnInit} from '@angular/core';
import {Filter} from '../../services/filter/filter';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {BookingsService} from '../../services/bookings/bookings.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {faCircleNotch} from '@fortawesome/free-solid-svg-icons';
import {SortDirection} from '../../services/filter/sort-direction';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {TeamsService} from '../../services/teams/teams.service';
import {TeamFilter} from '../../services/teams/team-filter';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss'],
})
export class TeamsListComponent implements OnInit {

  filter = new Filter(TeamFilter);
  teams: MyTeamResponse[];
  search: string;
  isLoading$ = new BehaviorSubject(true);
  faCircleNotch = faCircleNotch;

  constructor(
    private teamsService: TeamsService,
    private bookings: BookingsService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.filter.setLimit(50);
    this.filter.setSort('name', SortDirection.ASC);

    this.load();
  }

  load() {
    this.isLoading$.next(true);
    this.teamsService.getAllTeamsFiltered(this.filter).pipe(
    ).subscribe(resp => {
      this.teams = resp;
      this.isLoading$.next(false);
    });
  }

  tokenRegenerate(team: MyTeamResponse) {
    const dialogRef = this.dialog.open(RegenerateTeamInvitationTokenDialog, {
      maxWidth: '500px',
      width: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result === true) {
        this.teamsService.regenerateToken(team).subscribe(() => {
          this._snackBar.open('Invitation token for team "' + team.name + '" renewed!', null, {
            duration: 5000,
          });
          this.load();
        });
      }
    });
  }
}

@Component({
  selector: 'regenerate-team-invitation-token-dialog',
  template: `
    <h1 mat-dialog-title>Confirmation required
      <button class="fa-pull-right btn" aria-label="Close" [mat-dialog-close]="undefined"><span
        aria-hidden="true">x</span></button>
    </h1>
    <div mat-dialog-content>
      <p>All shared invitation links to your team will not be accessible after this action. This action cannot be undone!</p>
    </div>
    <div mat-dialog-actions>
      <button mat-button color="warn" [mat-dialog-close]="true">Confirm</button>
    </div>`,
})
export class RegenerateTeamInvitationTokenDialog {

  constructor(
    public dialogRef: MatDialogRef<RegenerateTeamInvitationTokenDialog>,
  ) {
  }

}
