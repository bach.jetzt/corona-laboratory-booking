import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Observable, Subject} from 'rxjs';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {filter, map, takeUntil, tap} from 'rxjs/operators';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {faDownload} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit, OnDestroy {

  personalReportForm = this.fb.group({
    'start': [{value: '', disabled: true}, Validators.compose([Validators.required])],
    'end': [{value: '', disabled: true}, Validators.compose([Validators.required])],
  });

  teamReportForms = this.fb.group({
    'forms': this.fb.array([]),
  });

  teamReportFilterResults = [];

  myTeams$: Observable<MyTeamResponse[]>;
  faDownload = faDownload;
  personalFrom: string;
  personalUntil: string;

  destroy$ = new Subject();

  constructor(
    private auth: AuthenticationService,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.myTeams$ = this.auth.authenticatedUser$.pipe(
      filter(u => u !== null),
      map(u => u.teams),
      map(teams => teams.filter(t => t.permission === 'OWNER')),
      tap(() => {
        if ((this.teamReportForms.get('forms') as FormArray).length) {
          (this.teamReportForms.get('forms') as FormArray).clear();
        }
      }),
      tap(teams => teams.forEach(team => {
        (this.teamReportForms.get('forms') as FormArray).push(this.fb.group({
          'start': [{value: '', disabled: true}, Validators.compose([Validators.required])],
          'end': [{value: '', disabled: true}, Validators.compose([Validators.required])],
        }));
        this.teamReportFilterResults.push({from: null, until: null});
      })),
    );

    this.personalReportForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap(c => {
        if (c.start !== '') {
          this.personalFrom = (new Date(c.start)).toISOString();
        }
        if (c.end !== '') {
          this.personalUntil = (new Date(c.end.getTime() + (1000 * 60 * 60 * 24))).toISOString();
        }
      }),
    ).subscribe();

    this.teamReportForms.valueChanges.pipe(
      takeUntil(this.destroy$),
      filter(f => f.forms.length > 0),
      map(f => f.forms),
      tap(forms => {
        forms.forEach((e, i) => {
          if (e.start !== '') {
            this.teamReportFilterResults[i].from = (new Date(e.start)).toISOString();
          }
          if (e.end !== '') {
            this.teamReportFilterResults[i].until = (new Date(e.end.getTime() + (1000 * 60 * 60 * 24))).toISOString();
          }
        });
      }),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
