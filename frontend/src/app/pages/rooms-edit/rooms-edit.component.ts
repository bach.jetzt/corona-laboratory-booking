import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomResponse} from '../../classes/rooms/room-response';
import {FormBuilder, Validators} from '@angular/forms';
import {UpdateRoom} from '../../classes/rooms/update-room';
import {RoomsService} from '../../services/rooms/rooms.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-rooms-edit',
  templateUrl: './rooms-edit.component.html',
  styleUrls: ['./rooms-edit.component.scss'],
})
export class RoomsEditComponent implements OnInit {

  room: RoomResponse;

  roomEditForm = this.fb.group({
    id: [''],
    team: [],
    number: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    floorName: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    building: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    allowedVisitors: ['', Validators.compose([Validators.required, Validators.min(0)])],
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private roomsService: RoomsService,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: { room: RoomResponse }) => {
        this.room = data.room;
        this.roomEditForm.patchValue(this.room);
      });
  }

  onSubmit() {
    const form = this.roomEditForm.getRawValue();
    const update: UpdateRoom = {
      number: form.number,
      floorName: form.floorName,
      building: form.building,
      allowedVisitors: form.allowedVisitors,
      teamId: form.team.id,
    };
    this.roomsService.updateRoom(this.room.id, update).subscribe(() => {
      this._snackBar.open('Updated room "' + this.room.number + '" successful!', null, {
        duration: 3000,
      });
      this.router.navigate(['/manage', 'rooms']);
    });
  }

  deleteRoom() {
    const dialogRef = this.dialog.open(DeleteRoomConfirmationDialog, {
      maxWidth: '500px',
      width: '80%',
      data: this.room,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.hasOwnProperty('id') && result.id === this.room.id) {
        this.roomsService.delete(this.room).subscribe(() => {
          this._snackBar.open('Delete room "' + this.room.number + '" successful!', null, {
            duration: 5000,
          });
          this.router.navigate(['/manage', 'rooms']);
        });
      }
    });
  }
}

@Component({
  selector: 'delete-room-confirmation-dialog',
  template: `
    <h1 mat-dialog-title>Confirmation required
      <button class="fa-pull-right btn" aria-label="Close" [mat-dialog-close]="undefined"><span
        aria-hidden="true">x</span></button>
    </h1>
    <div mat-dialog-content>
      <p>This action leads to data loss. To prevent accidental actions we ask you to confirm your intention.</p>
      <p>Please type the room number <span
        style="padding: 2px 4px; color: #1f1f1f; background-color: #f0f0f0; border-radius: 4px;">{{data.number}}</span>
        to proceed or close this dialog to cancel.</p>
      <form [formGroup]="roomDeleteForm">
        <div class="form-group">
          <input type="text" class="form-control" id="inputRoomConfirmation" formControlName="roomNumber"
                 [pattern]="data.number">
        </div>
      </form>
    </div>
    <div mat-dialog-actions>
      <button mat-button color="warn" [mat-dialog-close]="data" [disabled]="!roomDeleteForm.valid">Confirm</button>
    </div>`,
})
export class DeleteRoomConfirmationDialog {
  roomDeleteForm = this.fb.group({
    roomNumber: ['', Validators.compose([Validators.required])],
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteRoomConfirmationDialog>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: RoomResponse) {
  }
}
