import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {TeamResponse} from '../../classes/teams/team-response';
import {Observable, of, Subject} from 'rxjs';
import {TeamsService} from '../../services/teams/teams.service';
import {catchError, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {faCircleNotch} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-join-team',
  templateUrl: './join-team.component.html',
  styleUrls: ['./join-team.component.scss'],
})
export class JoinTeamComponent implements OnInit, OnDestroy {
  team: TeamResponse = null;
  teamstate: TeamState = 'UNDEFINED';
  finishedLoading$: Observable<boolean>;

  private destroy$ = new Subject();
  faCircleNotch = faCircleNotch;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private teams: TeamsService,
    private auth: AuthenticationService,
  ) {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnInit(): void {
    let team = this.route.paramMap.pipe(
      takeUntil(this.destroy$),
      switchMap((params: ParamMap) =>
        this.teams.getTeamByInvitationCode(params.get('invitationCode'))),
    );

    this.finishedLoading$ = team.pipe(
      tap(team => this.team = team),
      tap(() => this.teamstate = 'FOUND'),
      tap(team => {
        this.auth.authenticatedUser$.pipe(
          takeUntil(this.destroy$),
          tap(user => {
            const foundTeams = user.teams.filter(t => t.id === team.id);
            if (foundTeams.length > 0) {
              this.teamstate = 'ALREADY_JOINED';
            }
          }),
        ).subscribe();
      }),
      map(() => true),
      catchError(() => {
        this.teamstate = 'CODE_INVALID';
        return of(true);
      }),
    );
  }

  joinTeam(team: TeamResponse) {
    this.teams.joinTeam(team.id, team.invitationCode).pipe(
      tap(() => this.router.navigate(['/'])),
    ).subscribe();
  }
}

type TeamState = 'UNDEFINED' | 'ALREADY_JOINED' | 'CODE_INVALID' | 'FOUND';
