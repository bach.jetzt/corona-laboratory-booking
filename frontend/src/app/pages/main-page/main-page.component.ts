import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {NavigationTogglerService} from '../../services/navigation-toggler/navigation-toggler.service';
import {pairwise, startWith, takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {RoomFilterService} from '../../services/room-filter.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {
  faAngleLeft,
  faAngleRight,
  faBars,
  faCity,
  faHome,
  faMortarPestle,
  faSearch,
  faSignOutAlt,
  faSyncAlt,
  faTable,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import {faAddressBook} from '@fortawesome/free-regular-svg-icons';
import {SwUpdate} from '@angular/service-worker';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit, OnDestroy {
  sidebarToggled = false;
  searchForm = this.fb.group({
    search: [''],
  });
  user$ = this.auth.authenticatedUser$;
  faCity = faCity;
  faMortarPestle = faMortarPestle;
  faTable = faTable;
  faBars = faBars;
  faSearch = faSearch;
  faSignOutAlt = faSignOutAlt;
  faAddressBook = faAddressBook;
  faAngleRight = faAngleRight;
  faAngleLeft = faAngleLeft;
  faHome = faHome;
  faUsers = faUsers;
  faSync = faSyncAlt;
  pwaUpdateAvailable = false;
  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    public auth: AuthenticationService,
    private navToggler: NavigationTogglerService,
    private router: Router,
    private roomFilterService: RoomFilterService,
    private dialog: MatDialog,
    private swUpdate: SwUpdate,
  ) {
  }

  ngOnInit(): void {
    this.navToggler.isToggled$.pipe(
      takeUntil(this.destroy$),
    ).subscribe(state => {
      this.sidebarToggled = state;
    });
    this.searchForm.get('search').valueChanges.pipe(
      takeUntil(this.destroy$),
      startWith(''),
      pairwise(),
      tap(([p, n]: [string, string]) => {
          const extras = {};
          if (n !== '') {
            extras['queryParams'] = {search: n}
          }
          this.router.navigate(['/rooms/'], extras);
        },
      ),
    ).subscribe();

    this.roomFilterService.reset$.pipe(
      takeUntil(this.destroy$),
      tap(() => {
        this.searchForm.get('search').reset();
      }),
    ).subscribe();

    this.swUpdate.available.subscribe(u => {
      // Update was detected

      // Load Update
      this.swUpdate.activateUpdate().then(e => {
        // Update was load
        this.pwaUpdateAvailable = true;
      });
    });

    // Check for Pwa updates now
    if (this.swUpdate.isEnabled) {
      this.swUpdate.checkForUpdate();
    }
  }

  toggleSidebar() {
    this.navToggler.setToggleState(!this.sidebarToggled);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  askLogout() {
    const dialogRef = this.dialog.open(DialogLogoutComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((wantsToLogout: boolean) => {
      console.log('The dialog was closed, user wants to logout: ' + wantsToLogout);
      if (wantsToLogout) {
        this.auth.logout().subscribe(() => {
          this.router.navigate(['/auth', 'login']);
        });
      }
    });
  }

  reloadApp() {
    window.location.reload();
  }
}

@Component({
  selector: 'dialog-logout',
  template: `
    <h1 mat-dialog-title>Ready to Leave?</h1>
    <div mat-dialog-content>
      <p>Select "Logout" below if you are ready to end your current session and want to completely logout from this
        app.</p>
    </div>
    <div mat-dialog-actions>
      <button mat-button (click)="onNoClick()">Cancel</button>
      <button mat-button color="warn" [mat-dialog-close]="true">Logout</button>
    </div>
  `,
})
export class DialogLogoutComponent {

  constructor(public dialogRef: MatDialogRef<DialogLogoutComponent>) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
