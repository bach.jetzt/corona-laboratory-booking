import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {BookingsService} from '../../services/bookings/bookings.service';
import {BookingFilter} from '../../services/bookings/booking-filter';
import {Filter} from '../../services/filter/filter';
import {MatDialog} from '@angular/material/dialog';
import {faArrowRight, faCircleNotch} from '@fortawesome/free-solid-svg-icons';
import {CheckoutDialogComponent} from '../../dialogs/checkout-dialog/checkout-dialog.component';
import {BookingResponse} from '../../classes/bookings/booking-response';
import {SortDirection} from '../../services/filter/sort-direction';
import {RelationalOperator} from '../../services/filter/relational-operator';
import {tap} from 'rxjs/operators';
import {PagedResult} from '../../services/paged-result';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  recentBookings: PagedResult<BookingResponse>;
  user$ = this.auth.authenticatedUser$;
  currentBookings: BookingResponse[] = [];
  faCircleNotch = faCircleNotch;
  faArrowRight = faArrowRight;

  constructor(
    private auth: AuthenticationService,
    private _snackBar: MatSnackBar,
    private bookingService: BookingsService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.loadCurrentBookings();
    this.loadRecentBookings();
  }

  loadCurrentBookings() {
    const filter = new Filter(BookingFilter);
    filter.setSort('checkin', SortDirection.ASC);
    filter.setFilter('checkout', RelationalOperator.IS_NULL, 'true');
    this.bookingService.getAllBookingsFiltered(filter).subscribe(cb => this.currentBookings = cb);
  }

  loadRecentBookings() {
    const filter = new Filter(BookingFilter);
    filter.setLimit(6);
    filter.setSort('checkout', SortDirection.DESC);
    filter.setFilter('checkout', RelationalOperator.IS_NOT_NULL, 'true');
    this.bookingService.getFilteredBookings(filter).pipe(
      tap(res => this.recentBookings = res),
    ).subscribe();
  }

  checkout(booking: BookingResponse) {
    const dialogRef = this.dialog.open(CheckoutDialogComponent, {
      width: '270px',
      data: booking.room,
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      this.loadCurrentBookings();
      this.loadRecentBookings();
    });
  }
}
