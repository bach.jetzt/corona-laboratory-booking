import {Component, Inject, Input, OnInit} from '@angular/core';
import {MyTeamResponse} from '../../classes/teams/my-team-response';
import {faWhatsapp} from '@fortawesome/free-brands-svg-icons';
import {faQrcode, faShareAlt, faUsers} from '@fortawesome/free-solid-svg-icons';
import {NgNavigatorShareService} from 'ng-navigator-share';
import {environment} from '../../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent implements OnInit {
  @Input() team: MyTeamResponse;

  isMobile = true;

  faWhatsapp = faWhatsapp;
  faQrcode = faQrcode;
  faUsers = faUsers;
  faShareAlt = faShareAlt;

  invitationLink = environment.invitationLink;

  constructor(
    public shareService: NgNavigatorShareService,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
    private deviceDetectorService: DeviceDetectorService,
  ) {
  }

  ngOnInit(): void {
    if (this.deviceDetectorService.isDesktop()) {
      this.isMobile = false;
    }
  }

  showInvitationLinkCopied() {
    this._snackBar.open('Invitation link copied to clipboard!', null, {
      duration: 2500,
    });
  }

  openQrCode(code: string): void {
    const dialogRef = this.dialog.open(DialogInvitationLinkQrCode, {
      width: '280px',
      data: code,
    });
  }

}

@Component({
  selector: 'dialog-invitation-link-qr-code',
  template: `
    <div mat-dialog-content>
      <h3 class="text-gray-800 text-center">Show this QR code to your team members!</h3>
      <qrcode [qrdata]="link" [width]="230" [errorCorrectionLevel]="'M'" [elementType]="'svg'"
              colorDark="#333"></qrcode>
    </div>`,
})
export class DialogInvitationLinkQrCode {

  constructor(
    public dialogRef: MatDialogRef<DialogInvitationLinkQrCode>,
    @Inject(MAT_DIALOG_DATA) public link: string) {
  }

}
