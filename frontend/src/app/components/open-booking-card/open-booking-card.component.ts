import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {faSignOutAlt, faUser} from '@fortawesome/free-solid-svg-icons';
import {BookingResponse} from '../../classes/bookings/booking-response';

@Component({
  selector: 'app-open-booking-card',
  templateUrl: './open-booking-card.component.html',
  styleUrls: ['./open-booking-card.component.scss'],
})
export class OpenBookingCardComponent implements OnInit, OnChanges {
  faUser = faUser;
  faSignOutAlt = faSignOutAlt;
  usersOccupied = 0;
  usersFree = 0;
  usersOverLimit = 0;

  @Input() booking: BookingResponse;
  @Output() checkoutClicked = new EventEmitter<BookingResponse>();

  constructor() {
  }

  ngOnInit() {
    if (!this.booking) {
      throw new TypeError('\'booking\' is required');
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['booking']) {
      let booking: BookingResponse = changes['booking'].currentValue;
      this.usersFree = booking.room.allowedVisitors - booking.room.currentVisitors.length;
      if (this.usersFree < 0) {
        this.usersOverLimit = Math.abs(this.usersFree);
        this.usersFree = 0;
      }
      this.usersOccupied = booking.room.currentVisitors.length - this.usersOverLimit;
    }
  }

  checkout() {
    this.checkoutClicked.next(this.booking);
  }
}
