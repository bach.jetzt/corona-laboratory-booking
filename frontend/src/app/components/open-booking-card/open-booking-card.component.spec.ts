import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenBookingCardComponent } from './open-booking-card.component';

describe('OpenBookingCardComponent', () => {
  let component: OpenBookingCardComponent;
  let fixture: ComponentFixture<OpenBookingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenBookingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenBookingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
