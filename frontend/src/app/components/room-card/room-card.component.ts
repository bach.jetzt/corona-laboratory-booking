import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {faSignInAlt, faSignOutAlt, faStar as faStarSolid, faUser} from '@fortawesome/free-solid-svg-icons';
import {RoomResponse} from '../../classes/rooms/room-response';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {faStar} from '@fortawesome/free-regular-svg-icons';
import {RoomsService} from '../../services/rooms/rooms.service';
import {CheckinDialogComponent} from '../../dialogs/checkin-dialog/checkin-dialog.component';
import {CheckoutDialogComponent} from '../../dialogs/checkout-dialog/checkout-dialog.component';
import {BookingsService} from '../../services/bookings/bookings.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-room-card',
  templateUrl: './room-card.component.html',
  styleUrls: ['./room-card.component.scss'],
})
export class RoomCardComponent implements OnInit, OnChanges {
  faSignOutAlt = faSignOutAlt;
  faSignInAlt = faSignInAlt;
  faUser = faUser;
  faStarRegular = faStar;
  faStarSolid = faStarSolid;

  usersOccupied = 0;
  usersFree = 0;
  usersOverLimit = 0;

  alreadyCheckedIn = false;

  @Input() room: RoomResponse;
  @Output() checkinStateChanged = new EventEmitter<RoomResponse>();

  constructor(
    private authService: AuthenticationService,
    private bookingsService: BookingsService,
    private roomsService: RoomsService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    if (!this.room) {
      throw new TypeError('\'room\' is required');
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['room']) {
      let room: RoomResponse = changes['room'].currentValue;
      let currentUser = this.authService.authenticatedUser$.subscribe(u => {
        this.alreadyCheckedIn = u !== null && room.currentVisitors.filter(cv => {
          return u.id === cv.id;
        }).length > 0;
      });

      this.usersFree = room.allowedVisitors - room.currentVisitors.length;
      if (this.usersFree < 0) {
        this.usersOverLimit = Math.abs(this.usersFree);
        this.usersFree = 0;
      }
      this.usersOccupied = room.currentVisitors.length - this.usersOverLimit;
    }
  }

  checkin(room: RoomResponse) {
    const dialogRef = this.dialog.open(CheckinDialogComponent, {
      width: '250px',
      data: room,
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      if (result === undefined) {
        return;
      }

      let checkinTime: Date = null;
      if (result !== 'now') {
        checkinTime = new Date();
        checkinTime.setHours(parseInt(result.split(':')[0]));
        checkinTime.setMinutes(parseInt(result.split(':')[1]));
        checkinTime.setSeconds(0);
      }
      this.bookingsService.checkin(room, checkinTime).subscribe(() => {
        this.checkinStateChanged.next(room);
      });
    });
  }

  checkout(room: RoomResponse) {
    const dialogRef = this.dialog.open(CheckoutDialogComponent, {
      width: '270px',
      data: room,
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      this.checkinStateChanged.next(room);
    });
  }

  markAsFavorite(room: RoomResponse) {
    this.roomsService.markAsFavorite(room).subscribe();
  }

  removeFromFavorites(room: RoomResponse) {
    this.roomsService.removeFromFavorites(room).subscribe();
  }
}
