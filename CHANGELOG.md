# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0...1.0.1) (2020-07-26)


### Bug Fixes

* **autocheckout:** fixes autocheckout for open bookings with teams without autocheckout time defined ([91b682b](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/91b682b3896c0094d8840620731b79fcdbc297b3)), closes [#43](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/43)

## [1.0.0](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-rc.1...1.0.0) (2020-07-08)


### Features

* **permission:** permission management of team members ([fd4d5ae](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/fd4d5ae98e9a6071771198bf75f932b2ffdc9c53)), closes [#8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/8)

## [1.0.0-rc.1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-rc.0...1.0.0-rc.1) (2020-07-02)


### Bug Fixes

* **reports:** order bookings by building, floor and room name ([620d3f8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/620d3f8f11d16dbebc89757782bb86f30c3a2caa))

## [1.0.0-rc.0](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.7...1.0.0-rc.0) (2020-07-02)


### Features

* **booking:** force checkout bookings ([fb8295c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/fb8295c0c55b73152a33d945521a3e28f429f87a)), closes [#18](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/18)
* **export:** add pdf exports ([58bdf66](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/58bdf660ab0e38b4d93c2a0e806ecfcd4201776a)), closes [#32](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/32)

## [1.0.0-beta.7](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.6...1.0.0-beta.7) (2020-07-02)


### Features

* **app:** check for PWA updates and show update button if new version is available ([2031e78](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2031e78761ed623d5f322b1f4fb6da58ef5f3cc2))


### Bug Fixes

* **rooms:** fixes create route for rooms ([2f3b912](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2f3b9122395b80beae4e53f65dd2e8ccdf689ffa))

## [1.0.0-beta.6](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.5...1.0.0-beta.6) (2020-06-30)


### Features

* **favorites:** adds possibility to save rooms as favorites for faster booking ([a902998](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/a9029984df46eccef13bbb1c89390151f234d391)), closes [#37](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/37)
* **teams:** adds function to revoke memberships of team members ([83b097e](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/83b097edf1f1b1b1e032fca3114c1841be5cc863)), closes [#41](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/41)

## [1.0.0-beta.5](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.4...1.0.0-beta.5) (2020-06-26)


### Bug Fixes

* **auth:** fixes authenticator to use new login route ([71c995b](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/71c995ba4886c9909f1a9a7bb05e314acbba116a))

## [1.0.0-beta.4](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.3...1.0.0-beta.4) (2020-06-26)


### Features

* **security:** add configuration switch to enforce https on all routes ([aff0784](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/aff0784eae1c37c8937c56f1a76a3681b344fa0d)), closes [#42](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/42)
* **teams:** adds token renewal function ([39a45bc](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/39a45bc85f164612fe9b8a57888ee29b550e2484)), closes [#39](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/39)


### Bug Fixes

* **mailer:** use other sendmail configuration for prod deployment ([8103bba](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/8103bba6ddc661f591f29531b38e1467da1591a1))

## [1.0.0-beta.3](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.2...1.0.0-beta.3) (2020-06-24)


### Features

* **security:** adds password reset functionality into backend ([2eb5525](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2eb5525feaf336b6d5860dd7e60684afb054587b)), closes [#5](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/5)

## [1.0.0-beta.2](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.1...1.0.0-beta.2) (2020-06-22)


### Features

* **app:** rooms: polishing empty search response page ([7aaf11d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/7aaf11d84299882984615613ec9763dd67cb8dd2))
* **teams:** add create/update route ([6b6e96d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/6b6e96dfa04852ef8a3a074314f6c0434cbd9ed4)), closes [#38](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/38)
* **teams:** adds team management ui's ([9a9995c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/9a9995c4f6f46598cbfb2a9c57d30e6de05026ee)), closes [#36](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/36)


### Bug Fixes

* **app:** auth: when already logged in and nagivating to /auth pages -> automatic redirect to / ([0d546d4](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/0d546d4b0b8eafd3caf3e0649ed59ecbc8e5c4af))
* **app:** show team preview correctly shows the members count ([bea22c8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/bea22c8f9ebeb5b222e83826715d95c162b62a69))
* **fix:** fix ([8c32316](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/8c3231622261e0b0aba759371e891cf595583d6f))
* **teams:** fixes the MyTeamResponse to contain the teams users ([13f838e](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/13f838e00356e5a276fcc69436fc34f1b2a575c6))
* **validations:** fix filter usage ([7f70fe1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/7f70fe141e6fe4b287dc75e9d60b96a5f0030664))
* **validator:** use correct filter ([3a4825a](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/3a4825ac7d63ee3d72cad2fd0f280b2639fed095))

## [1.0.0-beta.1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/compare/1.0.0-beta.0...1.0.0-beta.1) (2020-06-19)


### Features

* **pagedresult:** return rooms and booking with correct http headers ([09e8cb2](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/09e8cb2a3b1b76e66803954abd6e343baf4fd1c9)), closes [#24](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/24)


### Bug Fixes

* **app:** manage rooms: paginator adjustments to new backend pagination ([83552b3](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/83552b3570106e9e2d780dd7a6c44e080136fc00)), closes [#35](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/35)

## 1.0.0-beta.0 (2020-06-18)


### Features

* **app:** adds favicon ([61a5698](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/61a56984ba29f7d712ed9b13b1beb165d70b5c20)), closes [#6](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/6)
* **app:** checkout is implemented now ([1957940](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/1957940b9cc20c3e6d100943d6da02127aa6dd85))
* **app:** dashboard: show recent finished bookings ([f77e32d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f77e32d903f52ed890f32b6c3499953188629f2b)), closes [#29](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/29)
* **app:** made a pwa from this web app ([cdc9a4e](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/cdc9a4e32413f63aabb60b4a60f70a9dc80a8b40))
* **app:** manage rooms: room can now be deleted ([ad1a3cd](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/ad1a3cd1089d8849ac12ed66f28517dc1c670fb9)), closes [#30](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/30)
* **app:** rooms: adds creation of rooms ([82faec1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/82faec12b38684fe17397424c64a65a3bbf6040e))
* **app:** top navi: adds home button on toggled side nav ([f40087c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f40087c64eeb3507ef20857844ef2e82b673ea3d))
* **booking:** add checkin and mybookings route ([b3b48f6](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/b3b48f66c1ed8a8f7a286d834bb591d105dca6e4))
* **bookings:** add configurable filter for completed bookings ([7590831](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/759083118a4e7830c2b857576f3d6f8b66e9a285)), closes [#14](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/14)
* **bookings:** add more filters ([c41cdf8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c41cdf826a515461363e5bb506d33b9667191bed)), closes [#26](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/26)
* **bookings:** add validation for checkout ([2861a6d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2861a6def663179709ef67e82b26d1dc9604e777)), closes [#27](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/27)
* **bookings:** add validation for checkout ([763fde3](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/763fde3d14b3299205ee43951b239f7047e6a6cd)), closes [#27](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/27)
* **bookings:** checkout booking ([b5a1738](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/b5a17381164edfcf3582fa2b8e6470bce716291f)), closes [#27](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/27)
* **bookings:** search for bookings ([fb84b61](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/fb84b6128ba5a666b9cdc6da7885249c57dec093))
* **checkin:** added checkin dialog with timepicker ([90e567f](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/90e567f834e644deb7019e51d5a7ab9652b6fc0b))
* **dashboard:** adds my current open bookings cards ([c2da618](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c2da618d25e7b78e13d15fd2f277a27d565d42dc))
* **dashboard:** mockup for dashboard finished ([3b765d9](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/3b765d9be168b05d432ff8747a0b7e54a6ddd814))
* **dashboard:** show text when not yet joined any team ([5a4c3fc](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/5a4c3fc8b9ff1557543a85393a5200b29f2cc890))
* **docker:** add web image for local testing ([f93ad24](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f93ad24328242853559cadd2d716e28c2d80facb))
* **docker:** fix volume path ([8420695](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/8420695cfc6fad5255dac23c6f78812a35a60035))
* **filter:** add more room filters ([ae65f01](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/ae65f01664fb0ff60a8b8e9f49d2db23eee043a8)), closes [#21](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/21)
* **filter:** add resolver for filter argument ([e59b538](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/e59b53872456cb73407813ba75c6d5c276e415c5))
* **filter:** implement first filter backend ([ac2e3e8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/ac2e3e859af1dd2f91f6c83fe05473153c6b72d8))
* **join-team:** adds join team page including error handling ([c26a298](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c26a298f88f9c0d593c4df77531e153f61f5229d))
* **login:** add user login ([630847f](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/630847f243fa2337d876c2a916ee0b120b1e7a71))
* **login:** handle remember me ([9eb9a74](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/9eb9a749feea82b41ff86c85eed82ab524a85da3))
* **logout:** adds logout capabilities for backend ([d469f09](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/d469f09cb46a32e8be718ee7c1c78206d5ee73bb)), closes [#4](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/4)
* **logout:** adds logout capability ([728957d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/728957da8c7e38b3f1add9cdfe26a89e0c6ac327)), closes [#4](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/4)
* **mapper:** add automapper ([25ef5d2](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/25ef5d289509586ca2200abab3d6796f1327b795))
* **register:** fixes register page ([2a01c47](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2a01c47d138bc3932a88962f9b05918736679517))
* **room:** delete room ([b11527d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/b11527d19c683f06509fb695ae4cef3bfec30e87)), closes [#22](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/22)
* **rooms:** add filter property ([cce5094](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/cce50944cb54ee3864c6990c5cf60d5a9ca076ca))
* **rooms:** adds rooms manage list ([4bfa692](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/4bfa692255fa291f7c2074587ed2176b1054f305))
* **rooms:** adjusts app to new room properties ([db668bc](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/db668bc1c886446c227a9a02aed49b83c860b5ab)), closes [#25](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/25)
* **rooms:** fix filter ([7a08300](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/7a08300c17bbe4751ba0d6e8eece67e054983376))
* **rooms:** fix repo ([9761402](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/9761402546468862cd50d6810c03560cd1ad5b47))
* **rooms:** show rooms with current bookings ([5ffecd9](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/5ffecd942d32a7a33449010e5c5283ea4770cf78)), closes [#3](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/3)
* **rooms:** update room ([edfeaa9](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/edfeaa9636f1c86e3b45792721d02b867756e926)), closes [#9](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/9)
* **rooms-manager:** adds edit page for room configuration ([953d2c8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/953d2c8ecfa7e34ea4235119afc1cfcfd99d281f))
* **serializer:** add automatic serialization of body ([16d18ea](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/16d18ea13143d9662287c640ecadd5d807dff7f2))
* **skeleton:** add initial commit ([a8bd97e](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/a8bd97eaced11ac0286bbad03c63a7b6fda3dc30))
* **team:** add property forcedCheckoutTime ([c783a27](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c783a27c3d7ecd72b1216e367401e3325861fb5c)), closes [#17](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/17)
* **teams:** adds pages for team invitation (backend interaction is not yet implemented) ([2243640](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/22436405121c9b5d8b71d9a1f9ecb87ef50736c0))
* **teams:** get information by invitation code ([18db64c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/18db64c228781a34d9581e247ffda76cb3049cfc)), closes [#1](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/1)
* **teams:** get rooms ([48718ff](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/48718ff6fb4fced46b361d8d81126cd92b28132c)), closes [#7](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/7)
* **teams:** invitation link sharing via QR code ([67975dc](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/67975dccb62d04c3dc35fd77fac0a634f99b26fe)), closes [#16](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/16)
* **teams:** join team with invitation code ([f356a1c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f356a1c531eddd91c3b63ff760a2b895c63a404f)), closes [#2](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/2)
* **ui:** adds initial angular frontend ([c58498b](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c58498b104a2de35d85e0e5613d96335c2fa8d0a))
* **users:** fix register route ([79fd1de](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/79fd1debf03d8d211b9c04cddc8921aa55cda744)), closes [#12](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/12)
* **validation:** add first database validator ([f222ab9](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f222ab9e39ba69f08c053e76f31d761f8ccb3b4f))
* **validation:** checking is using bookable annotation ([ec461e7](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/ec461e7488d7b3e10bcca23e1c3ed926dc2d6c48)), closes [#20](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/20)


### Bug Fixes

* **app:** adjustments to new backend filter/sorting ([abaef15](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/abaef15cfb4c6c7730f91064965e843a9bf289d9))
* **app:** checkout: adds validation for user custom input validation ([03cba31](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/03cba31f7cc58c91f47e09de1263a612440c8b12)), closes [#28](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/28)
* **app:** checkout: fixes an error with time ranges from 0-9 minutes ([6650784](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/6650784a45c9fb03eede47ece72d1f0c277d7e7c))
* **app:** fixes some wordings and styling issues ([f4d0376](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f4d0376718823410d959b2a832b36e91cc48e6a4))
* **app:** rooms configurator ([d2273be](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/d2273be74875ec9f1ffa16df0595e3b38e514a3c))
* **app:** small fixes ([c7b1fb2](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c7b1fb280eb55119eb69e3365e39ce7f7cfd4a47))
* **cleanup:** mapping ([e5e6223](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/e5e6223f8da094102bdd33c739e059c804d9d681))
* **cleanup:** remove unneeded files ([4e39a29](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/4e39a29d2c290cef07f3b5fed0dde082131c004b))
* **db:** add tables and fields ([f73509d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f73509d155b64b7277d36c8e53892f465d6c1554))
* **db:** pin to mysql 5.7.30 ([891cca8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/891cca82b11e2b19fd7ff27487e9f697238c5bd6))
* **db:** use mysql 8 in compose file ([1478a62](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/1478a62a12a798bb7d7f0e0326b2400bcf9e5527))
* **doctrine:** remove eager setting ([0d550f6](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/0d550f6e7061fd73cb583aa35e5db18f8c17b678)), closes [#11](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/11)
* **entities:** remove eager fetching ([45f0fac](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/45f0fac45fb39270e93cd440b295ecad53f943b9))
* **filter:** handle filter parsing ([c771b3d](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/c771b3de2733b2d92b09828c4d1a161dd90e7b51))
* **fix:** fix ([1776223](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/17762239dd8625216e515b942993af1d48e72b01))
* **login:** return myuserresponse after login ([de6ea88](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/de6ea88749a7e17fc4c2b4471303db3baa83c11b))
* **mapper:** add configuration ([2da8fc3](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/2da8fc39378c7374ae0d8fc924b483ba2e231df3))
* **namespace:** fix namespaces ([f58cc59](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/f58cc5909b9fa1d43a5d9b34d3b30a2e90d152a2))
* **naming:** removes corona from app name ([cfd8d42](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/cfd8d42275706cef50756aac108fb1104af50706))
* **orm:** working on entities ([b815ca8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/b815ca8191801387fe1ff7bd255b896a7874dfae))
* **rest:** add me route ([9edde4c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/9edde4caaa1953a50c3ad7e2dac6ddb5145ecffd))
* **rest:** add register user route ([1b8d018](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/1b8d01809ed812c41a4fb5cf98a6e67f29f1111d))
* **room:** fix filter ([77b8897](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/77b889745612c66df474c480ebab4dcd63b8640f))
* **room:** update properties ([644140a](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/644140a735db8e9ffec820f451a90e6ac3a19d48)), closes [#25](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/25)
* **rooms:** fix filter ([880131e](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/880131ebc0d3d4bfbdd8ca68b70b0b01ad2e81ca))
* **rooms:** fixes rooms filter ([1636cb8](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/1636cb839dcf61482231e963e870d8a4f35d82a8))
* **rooms:** improve creation route ([e5f14ee](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/e5f14eec0cf408dd3fd84f1d76380bdb77f20e35)), closes [#10](https://gitlab.com/bach.jetzt/corona-laboratory-booking/issues/10)
* **rooms-checkin:** several small improvements ([ace872f](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/ace872fac44d9aa524558a2d470233c8a0b01061))
* **rooms-checking:** improved building-floor view ([9b4b9ed](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/9b4b9ed3607d8c7d127cebea8dcea4b19cde827f))
* **rooms-manage:** disable page on very small devices ([dc83643](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/dc836436ae582a72026aba5cad7768db04db1504))
* **security:** fix log in ([cb7b291](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/cb7b291bb8397b6883df84394fb860c791aec69c))
* **session:** make rememberMe selectable ([38fbf15](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/38fbf15bf87a5d3c017c48e28a07ddaf9212e661))
* **ui:** fixes timepicker when checking into a room ([26a24d4](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/26a24d4f42d704d88a85ce122e18627443dca791))
* **version:** upgrade to 5.1 ([662781c](https://gitlab.com/bach.jetzt/corona-laboratory-booking/commit/662781c74b33e1fbc8487b8275fdf4993b80d263))
