<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * @Annotation
 */
class Bookable extends Constraint
{
    const NOT_BOOKABLE_ERROR = 'd1796a98-6edc-42a2-8b41-09ff6e5da820';
    const NOT_BOOKABLE_FOR_CHECKOUT_ERROR = '46edf0cc-6b1c-4fb6-9d3e-1d3b7635bb51';
    public $checkin_message = 'You are already checked in into this room.';
    public $checkout_message = 'You are not booked for this room.';

    public string $type;

    public function __construct($options = null)
    {
        if (!in_array($options['type'], ['checkin', 'checkout'])) {
            throw new InvalidArgumentException(sprintf('Option "type" must be set either as "checkin" or "checkout".'), ['type']);
        }
        $this->type = $options['type'];

        parent::__construct($options);
    }

    public function getDefaultOption(): ?string
    {
        return 'type';
    }
}
