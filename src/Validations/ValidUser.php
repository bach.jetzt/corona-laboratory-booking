<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidUser extends Constraint
{
    const INVALID_USER_ERROR = '09ad13b8-1ef0-4de8-a5c0-4a5a0c8ff6f5';
    public $message = 'This is not a valid user id.';
}
