<?php

namespace App\Validations;

use App\Repository\TeamRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidInvitationCodeValidator extends ConstraintValidator
{
    private TeamRepository $teamRepo;

    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepo = $teamRepository;
    }

    /**
     * @param mixed $value invitationCode
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidInvitationCode) {
            throw new UnexpectedTypeException($constraint, ValidInvitationCode::class);
        }
        $found = $this->teamRepo->findOneBy(['invitationCode' => $value]);
        if ($found === null) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(ValidInvitationCode::INVALID_INVITATION_CODE_ERROR)
                ->addViolation();
        }
    }
}
