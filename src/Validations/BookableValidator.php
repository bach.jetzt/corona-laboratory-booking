<?php

namespace App\Validations;

use App\Entity\User;
use App\Repository\BookingRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BookableValidator extends ConstraintValidator
{
    private BookingRepository $bookingRepo;
    private Security $security;

    public function __construct(BookingRepository $bookingRepository, Security $security)
    {
        $this->bookingRepo = $bookingRepository;
        $this->security = $security;
    }

    /**
     * @param mixed $value roomId
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof Bookable) {
            throw new UnexpectedTypeException($constraint, Bookable::class);
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $openBookings = $this->bookingRepo->findOpenBookings($user, $value);

        switch ($constraint->type) {
            case 'checkin':
                if (count($openBookings) > 0) {
                    $this->context->buildViolation($constraint->checkin_message)
                        ->setParameter('{{ value }}', $this->formatValue($value))
                        ->setCode(Bookable::NOT_BOOKABLE_ERROR)
                        ->addViolation();
                }
                break;
            case 'checkout':
                if (count($openBookings) < 1) {
                    $this->context->buildViolation($constraint->checkout_message)
                        ->setParameter('{{ value }}', $this->formatValue($value))
                        ->setCode(Bookable::NOT_BOOKABLE_FOR_CHECKOUT_ERROR)
                        ->addViolation();
                }
                break;
        }
    }
}
