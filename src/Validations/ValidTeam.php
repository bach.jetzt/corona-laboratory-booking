<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidTeam extends Constraint
{
    const INVALID_TEAM_ERROR = 'c35658b1-df40-47b2-94a3-649d1bf22922';
    public $message = 'This is not a valid team id.';
}
