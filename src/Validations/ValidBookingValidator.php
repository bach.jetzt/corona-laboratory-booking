<?php

namespace App\Validations;

use App\Entity\User;
use App\Filter\BookingFilter;
use App\Filter\Operators\Equals;
use App\Repository\BookingRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidBookingValidator extends ConstraintValidator
{
    private BookingRepository $bookingRepo;
    private Security $security;

    public function __construct(BookingRepository $bookingRepo, Security $security)
    {
        $this->bookingRepo = $bookingRepo;
        $this->security = $security;
    }

    /**
     * @param mixed $value bookingId
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidBooking) {
            throw new UnexpectedTypeException($constraint, ValidBooking::class);
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $bookingFilter = new BookingFilter();
        $bookingFilter->id[] = new Equals($value);
        $pagedResult = $this->bookingRepo->getFiltered($user, $bookingFilter);


        if ($pagedResult->totalCount !== 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(ValidBooking::INVALID_BOOKING_ERROR)
                ->addViolation();
        }
    }
}
