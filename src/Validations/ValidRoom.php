<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidRoom extends Constraint
{
    const INVALID_ROOM_ERROR = '59a9e7a9-3f47-4d7d-9c32-bbcd0eb53105';
    public $message = 'This is not a valid room id.';
}
