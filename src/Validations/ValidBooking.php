<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidBooking extends Constraint
{
    const INVALID_BOOKING_ERROR = '1560391f-2706-40cf-bf2b-4506bc920d85';
    public $message = 'This is not a valid booking id.';
}
