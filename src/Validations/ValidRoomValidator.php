<?php

namespace App\Validations;

use App\Entity\User;
use App\Filter\Operators\Equals;
use App\Filter\RoomFilter;
use App\Filter\TeamFilter;
use App\Repository\RoomRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidRoomValidator extends ConstraintValidator
{
    private RoomRepository $roomRepo;
    private Security $security;

    public function __construct(RoomRepository $roomRepository, Security $security)
    {
        $this->roomRepo = $roomRepository;
        $this->security = $security;
    }

    /**
     * @param mixed $value roomId
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidRoom) {
            throw new UnexpectedTypeException($constraint, ValidRoom::class);
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $roomFilter = new RoomFilter();
        $roomFilter->id[] = new Equals($value);
        $pagedResult = $this->roomRepo->getFiltered($user, $roomFilter);


        if ($pagedResult->totalCount !== 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(ValidRoom::INVALID_ROOM_ERROR)
                ->addViolation();
        }
    }
}
