<?php

namespace App\Validations;

use App\Entity\User;
use App\Filter\Operators\Equals;
use App\Filter\TeamFilter;
use App\Repository\TeamRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidTeamValidator extends ConstraintValidator
{
    private TeamRepository $teamRepo;
    private Security $security;

    public function __construct(TeamRepository $teamRepository, Security $security)
    {
        $this->teamRepo = $teamRepository;
        $this->security = $security;
    }

    /**
     * @param mixed $value teamId
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidTeam) {
            throw new UnexpectedTypeException($constraint, ValidTeam::class);
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $teamFilter = new TeamFilter();
        $teamFilter->id[] = new Equals($value);
        $pagedResult = $this->teamRepo->getFiltered($teamFilter);

        if ($pagedResult->totalCount !== 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(ValidTeam::INVALID_TEAM_ERROR)
                ->addViolation();
        }
    }
}
