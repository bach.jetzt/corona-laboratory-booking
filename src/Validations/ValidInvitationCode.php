<?php

namespace App\Validations;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidInvitationCode extends Constraint
{
    const INVALID_INVITATION_CODE_ERROR = '5af084bb-936a-48b9-a274-298548fd269c';
    public $message = 'The invitation code is invalid.';
}
