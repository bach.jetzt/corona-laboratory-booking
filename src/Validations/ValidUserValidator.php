<?php

namespace App\Validations;

use App\Entity\User;
use App\Filter\Operators\Equals;
use App\Filter\UserFilter;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ValidUserValidator extends ConstraintValidator
{
    private UserRepository $userRepo;
    private Security $security;

    public function __construct(UserRepository $userRepository, Security $security)
    {
        $this->userRepo = $userRepository;
        $this->security = $security;
    }

    /**
     * @param mixed $value userId
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ValidUser) {
            throw new UnexpectedTypeException($constraint, ValidUser::class);
        }
        /** @var User $user */
        $user = $this->security->getUser();
        $userFilter = new UserFilter();
        $userFilter->id[] = new Equals($value);
        $pagedResult = $this->userRepo->getFiltered($userFilter);

        if ($pagedResult->totalCount !== 1) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(ValidUser::INVALID_USER_ERROR)
                ->addViolation();
        }
    }
}
