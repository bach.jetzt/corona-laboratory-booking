<?php

namespace App\Service\Security;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{

    private AutoMapperInterface $mapper;

    /**
     * LoginSuccessHandler constructor.
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    public function onLogoutSuccess(Request $request)
    {
        return new JsonResponse();
    }
}
