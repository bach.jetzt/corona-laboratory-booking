<?php

namespace App\Service\Security;

use App\Entity\User;
use App\Messages\MyUserResponse;
use App\Repository\UserRepository;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private AutoMapperInterface $mapper;
    private NormalizerInterface $normalizer;
    private UserRepository $userRepository;

    /**
     * LoginSuccessHandler constructor.
     * @param AutoMapperInterface $mapper
     * @param NormalizerInterface $normalizer
     * @param UserRepository $userRepository
     */
    public function __construct(
        AutoMapperInterface $mapper,
        NormalizerInterface $normalizer,
        UserRepository $userRepository
    )
    {
        $this->mapper = $mapper;
        $this->normalizer = $normalizer;
        $this->userRepository = $userRepository;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        $this->userRepository->updatePasswordResetToken($user, null);

        $mappedData = $this->mapper->map($token->getUser(), MyUserResponse::class);
        $body = $this->normalizer->normalize($mappedData, 'json');
        return new JsonResponse($body);
    }
}
