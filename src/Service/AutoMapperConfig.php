<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\Room;
use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserRoomFavorite;
use App\Entity\UserTeam;
use App\Messages\BookingResponse;
use App\Messages\CreateRoom;
use App\Messages\CreateTeam;
use App\Messages\MyTeamResponse;
use App\Messages\MyUserResponse;
use App\Messages\RoomResponse;
use App\Messages\TeamMember;
use App\Messages\TeamResponse;
use App\Messages\UpdateRoom;
use App\Messages\UpdateTeam;
use App\Messages\UserResponse;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use Monolog\DateTimeImmutable;

class AutoMapperConfig implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->getOptions()->setPropertyAccessor(new CustomDoctrineProxyPropertyAccessor());

        $config
            ->registerMapping(CreateRoom::class, Room::class)
            ->forMember('id', Operation::ignore())
            ->forMember('bookings', Operation::setTo([]));

        $config
            ->registerMapping(UpdateRoom::class, Room::class);

        $config
            ->registerMapping(Room::class, RoomResponse::class)
            ->forMember('team', Operation::mapTo(TeamResponse::class))
            ->forMember(
                'currentVisitors', function (Room $room, AutoMapperInterface $mapper) {
                $currentBookings = $room->getBookings()->filter(fn(Booking $b) => $b->getCheckout() === null);
                return $mapper->mapMultiple(array_map(fn(Booking $booking) => $booking->getUser(), $currentBookings->getValues()), UserResponse::class);
            }
            );

        $config
            ->registerMapping(Team::class, TeamResponse::class)
            ->forMember('roomsCount', fn(Team $team) => count($team->getRooms()))
            ->forMember('membersCount', fn(Team $team) => count($team->getUserTeams()));

        $config
            ->registerMapping(Team::class, MyTeamResponse::class)
            ->forMember('roomsCount', fn(Team $team) => count($team->getRooms()))
            ->forMember('membersCount', fn(Team $team) => count($team->getUserTeams()))
            ->forMember('members', function (Team $team, AutoMapperInterface $mapper) {
                $userTeams = $team->getUserTeams();
                return $mapper->mapMultiple($userTeams->getValues(), TeamMember::class);
            })
            ->forMember('forcedCheckoutTime', fn(Team $team) => $team->getForcedCheckoutTime() !== null ? $team->getForcedCheckoutTime()->setDate((new DateTimeImmutable('now'))->format('Y'), (new DateTimeImmutable('now'))->format('m'), (new DateTimeImmutable('now'))->format('d')):null);

        $config
            ->registerMapping(UserTeam::class, TeamResponse::class)
            ->forMember('id', fn(UserTeam $team) => $team->getTeam()->getId())
            ->forMember('name', fn(UserTeam $team) => $team->getTeam()->getName())
            ->forMember('invitationCode', fn(UserTeam $team) => $team->getTeam()->getInvitationCode())
            ->forMember('roomsCount', fn(UserTeam $team) => count($team->getTeam()->getRooms()))
            ->forMember('membersCount', fn(UserTeam $userTeam) => count($userTeam->getTeam()->getUserTeams()));

        $config
            ->registerMapping(UserTeam::class, MyTeamResponse::class)
            ->forMember('id', fn(UserTeam $userTeam) => $userTeam->getTeam()->getId())
            ->forMember('name', fn(UserTeam $userTeam) => $userTeam->getTeam()->getName())
            ->forMember('forcedCheckoutTime', fn(UserTeam $userTeam) => $userTeam->getTeam()->getForcedCheckoutTime() !== null ? $userTeam->getTeam()->getForcedCheckoutTime()->setDate((new DateTimeImmutable('now'))->format('Y'), (new DateTimeImmutable('now'))->format('m'), (new DateTimeImmutable('now'))->format('d')):null)
            ->forMember('invitationCode', fn(UserTeam $userTeam) => $userTeam->getTeam()->getInvitationCode())
            ->forMember('roomsCount', fn(UserTeam $userTeam) => count($userTeam->getTeam()->getRooms()))
            ->forMember('permission', fn(UserTeam $userTeam) => $userTeam->getPermission())
            ->forMember('members', function (UserTeam $userTeam, AutoMapperInterface $mapper) {
                $userTeams = $userTeam->getTeam()->getUserTeams();
                return $mapper->mapMultiple(array_map(fn(UserTeam $ut) => $ut->getUser(), $userTeams->getValues()), UserResponse::class);
            });

        $config
            ->registerMapping(UserTeam::class, TeamMember::class)
            ->forMember('user', Operation::fromProperty('user')->mapTo(UserResponse::class))
            ->forMember('permission', fn(UserTeam $userTeam) => $userTeam->getPermission());

        $config
            ->registerMapping(CreateTeam::class, Team::class)
            ->forMember('rooms', Operation::setTo([]))
            ->forMember('userTeams', Operation::setTo([]));

        $config
            ->registerMapping(UpdateTeam::class, Team::class);

        $config
            ->registerMapping(User::class, UserResponse::class)
            ->forMember('roles', fn(User $user) => $user->getRoles());

        $config
            ->registerMapping(User::class, MyUserResponse::class)
            ->forMember('teams', Operation::fromProperty('userTeams')->mapTo(MyTeamResponse::class))
            ->forMember('favoriteRooms', function (User $user, AutoMapperInterface $mapper) {
                return $mapper->mapMultiple(array_map(fn(UserRoomFavorite $urf) => $urf->getRoom(), $user->getUserRoomFavorites()->getValues()), RoomResponse::class);
            })
            ->forMember('roles', fn(User $user) => $user->getRoles());

        $config
            ->registerMapping(Booking::class, BookingResponse::class)
            ->forMember('room', Operation::mapTo(RoomResponse::class));
    }
}
