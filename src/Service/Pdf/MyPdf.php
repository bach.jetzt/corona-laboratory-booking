<?php

namespace App\Service\Pdf;

use App\Entity\PseudoEntity\PseudoBooking;
use App\Entity\PseudoEntity\PseudoBookingTeam;
use TCPDF;

class MyPdf extends TCPDF
{
    public function Header(): void
    {
        // Logo
        $image_file = __DIR__ . '/../../../public/assets/favicon/android-chrome-512x512.png';
        $this->Image($image_file, 15, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 24);
        // Title
        $this->Write(15, 'LabBooking.App', 0, false, 'C', true, 0, false, false, 0);
    }

    // Page footer
    public function Footer(): void
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    // Colored table

    /**
     * @param $header
     * @param PseudoBooking[] $data
     */
    public function ColoredTable($header, $data): void
    {
        $this->SetFont('helvetica', '', 12);

        $tbl = '<table cellspacing="0" cellpadding="5" border="1"><tr style="background-color: #4e73df; color: white; font-weight: bold;">';
        foreach ($header as $h) {
            $tbl .= '<td>' . $h . '</td>';
        }
        $tbl .= '</tr>';

        foreach ($data as $row) {
            $checkin = $row->checkin->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y H:i:s');
            $checkout = $row->checkout === null ? null : $row->checkout->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y H:i:s');
            $tbl .= '<tr>';
            $tbl .= '<td>' . $row->roomNumber . '</td>';
            $tbl .= '<td>' . $row->floorName . '</td>';
            $tbl .= '<td>' . $row->building . '</td>';
            $tbl .= '<td>' . $checkin . '</td>';
            $tbl .= '<td>' . $checkout . '</td>';
            $tbl .= '<td>' . implode('<br>', $row->encounters) . '</td>';

            $tbl .= '</tr>';
        }

        $tbl .= '</table>';

        $this->writeHTML($tbl, true, false, false, false, '');
    }

    /**
     * @param PseudoBookingTeam[][] $data
     */
    public function teamTable(array $data, string $fromString, string $untilString): void
    {
        $textWidth = $this->getPageWidth() - $this->getMargins()['left'] - $this->getMargins()['right'];
        $half = $textWidth / 2;
        foreach ($data as $room => $roomBookings) {
            $this->SetFont('helvetica', 'B', 18);
            $this->AddPage();
            $this->Ln(5);
            $this->MultiCell($half, 15, $room, 0, 'L', 0, 0, '', '', true);
            $this->MultiCell($half, 15, $fromString . ' - ' . $untilString, 0, 'R', 0, 1, '', '', true);
            $this->SetFont('helvetica', '', 12);
            $tbl = '<table cellspacing="0" cellpadding="5" border="1"><tr style="background-color: #4e73df; color: white; font-weight: bold;">';
            foreach (['Name', 'Checkin', 'Checkout'] as $h) {
                $tbl .= '<td>' . $h . '</td>';
            }
            $tbl .= '</tr>';
            foreach ($roomBookings as $booking) {
                $checkin = $booking->checkin->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y H:i:s');
                $checkout = $booking->checkout === null ? null : $booking->checkout->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y H:i:s');
                $tbl .= '<tr>';
                $tbl .= '<td>' . $booking->userName . '</td>';
                $tbl .= '<td>' . $checkin . '</td>';
                $tbl .= '<td>' . $checkout . '</td>';

                $tbl .= '</tr>';
            }
            $tbl .= '</table>';
            $this->writeHTML($tbl, true, false, false, false, '');
        }
    }
}
