<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class LostPassword
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $username;
}
