<?php

namespace App\Messages;

use App\Validations\Bookable;
use App\Validations\ValidRoom;
use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

class Checkin
{
    /**
     * @Assert\Uuid()
     * @Assert\NotNull()
     * @ValidRoom()
     * @Bookable(type="checkin")
     */
    public string $roomId;

    public ?DateTimeImmutable $time = null;
}
