<?php

namespace App\Messages;

class MyUserResponse
{
    public string $id;
    public string $email;
    public string $name;
    /**
     * @var MyTeamResponse[]
     */
    public array $teams;
    /**
     * @var RoomResponse[]
     */
    public array $favoriteRooms;
    /**
     * @var string[]
     */
    public array $roles;
}
