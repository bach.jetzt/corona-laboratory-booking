<?php

namespace App\Messages;

class RoomResponse
{
    public string $id;
    public string $number;
    public ?string $floorName;
    public string $building;
    public int $allowedVisitors;
    /**
     * @var UserResponse[]
     */
    public array $currentVisitors;
    public TeamResponse $team;
}
