<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class CreateRoom
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ?string $number;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ?string $floorName;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ?string $building;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    public ?int $allowedVisitors;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $teamId;
}
