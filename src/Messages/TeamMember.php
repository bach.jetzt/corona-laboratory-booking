<?php

namespace App\Messages;

class TeamMember
{
    public UserResponse $user;
    public string $permission;
}
