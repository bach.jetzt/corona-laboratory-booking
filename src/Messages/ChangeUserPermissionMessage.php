<?php

namespace App\Messages;

use App\Validations\ValidUser;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeUserPermissionMessage
{
    /**
     * @Assert\NotNull()
     * @ValidUser()
     */
    public string $userId;

    /**
     * @Assert\Choice(App\Entity\UserTeam::PERMISSIONS)
     * @Assert\NotNull()
     */
    public string $newPermission;
}
