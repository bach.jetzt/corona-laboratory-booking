<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class CreateUser
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $email;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $name;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $password;
}
