<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateRoom
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $number;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $floorName;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public ?string $building;
    /**
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(0)
     */
    public int $allowedVisitors;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $teamId;
}
