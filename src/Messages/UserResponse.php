<?php

namespace App\Messages;

class UserResponse
{
    public string $id;
    public string $email;
    public string $name;
}
