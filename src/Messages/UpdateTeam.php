<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateTeam
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=50)
     */
    public string $name;
    public ?\DateTimeImmutable $forcedCheckoutTime = null;
}
