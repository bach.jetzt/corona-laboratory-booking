<?php

namespace App\Messages;

use DateTimeImmutable;

class BookingResponse
{
    public string $id;
    public RoomResponse $room;
    public string $userId;
    public DateTimeImmutable $checkin;
    public ?DateTimeImmutable $checkout = null;
}
