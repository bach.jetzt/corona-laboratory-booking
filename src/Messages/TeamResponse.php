<?php

namespace App\Messages;

class TeamResponse
{
    public string $id;
    public string $name;
    public string $invitationCode;
    public int $roomsCount;
    public int $membersCount;
}
