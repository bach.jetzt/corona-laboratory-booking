<?php

namespace App\Messages;

use DateTimeImmutable;

class MyTeamResponse extends TeamResponse
{
    public ?string $permission;
    public ?DateTimeImmutable $forcedCheckoutTime;
    /**
     * @var TeamMember[]
     */
    public array $members;
}
