<?php

namespace App\Messages;

use Symfony\Component\Validator\Constraints as Assert;

class LostPasswordReset
{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $resetToken;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public string $password;
}
