<?php

namespace App\ArgumentResolver;

use App\EventListener\RequestValidationException;
use App\Filter\AbstractFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FilterResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;
    private DenormalizerInterface $denormalizer;

    public function __construct(ValidatorInterface $validator, DenormalizerInterface $denormalizer)
    {
        $this->validator = $validator;
        $this->denormalizer = $denormalizer;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getName() === 'filter' && is_subclass_of($argument->getType(), AbstractFilter::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $filterType = $argument->getType();
        $query = $request->query->all();
        $filter = $this->denormalizer->denormalize($query, $filterType);

        $validationResult = $this->validator->validate($filter);

        if ($validationResult->count() > 0) {
            throw new RequestValidationException($validationResult, 'Validation failed, please check query');
        }

        yield $filter;
    }
}
