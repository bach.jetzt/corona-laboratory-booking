<?php

namespace App\ArgumentResolver;

use App\Filter\AbstractFilter;
use App\Filter\Operators\Equals;
use App\Filter\Operators\GreaterThan;
use App\Filter\Operators\GreaterThanOrEquals;
use App\Filter\Operators\In;
use App\Filter\Operators\IsNotNull;
use App\Filter\Operators\IsNull;
use App\Filter\Operators\LessThan;
use App\Filter\Operators\LessThanOrEquals;
use App\Filter\Operators\Like;
use App\Filter\Operators\NotEquals;
use App\Filter\Operators\NotIn;
use App\Filter\Sorting\Ascending;
use App\Filter\Sorting\Descending;
use DateTimeZone;
use Psr\Log\LoggerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class FilterDenormalizer implements DenormalizerInterface
{
    private const operatorMapping = [
        Ascending::DIRECTION => Ascending::class,
        Descending::DIRECTION => Descending::class,
        Equals::OPERATOR => Equals::class,
        NotEquals::OPERATOR => NotEquals::class,
        LessThan::OPERATOR => LessThan::class,
        LessThanOrEquals::OPERATOR => LessThanOrEquals::class,
        GreaterThan::OPERATOR => GreaterThan::class,
        GreaterThanOrEquals::OPERATOR => GreaterThanOrEquals::class,
        In::OPERATOR => In::class,
        NotIn::OPERATOR => NotIn::class,
        IsNull::OPERATOR => IsNull::class,
        IsNotNull::OPERATOR => IsNotNull::class,
        Like::OPERATOR => Like::class,
    ];

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        /** @var AbstractFilter $finalData */
        $finalData = new $type;
        foreach ($data as $key => $values) {
            if (!property_exists($type, $key)) {
                continue;
            }
            if ($key === 'limit') {
                $finalData->setLimit((int)$values);
                continue;
            }
            if ($key === 'offset') {
                $finalData->setOffset((int)$values);
                continue;
            }
            if (!is_array($values)) {
                $values = ['eq' => $values];
            }
            foreach ($values as $operator => $value) {
                if ($key === 'sortings') {
                    // $value = ASC, $operator = name
                    $sortingClass = self::operatorMapping[strtolower($value)];
                    $finalData->addSorting(new $sortingClass($operator));
                    continue;
                }
                // $operator = eq, $value = <searchstring>, $key = name
                $filterClass = self::operatorMapping[strtolower($operator)];
                $finalData->$key[] = (new $filterClass($this->transformValue($value)));
            }
        }
        return $finalData;
    }

    private function transformValue($value)
    {
        return $value;
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return is_subclass_of($type, AbstractFilter::class, true);
    }
}
