<?php

namespace App\ArgumentResolver;

use App\EventListener\RequestValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JsonBodyResolver implements ArgumentValueResolverInterface
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $argument->getName() === 'payload' && class_exists($argument->getType());
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $deserializedBody = null;
        try {
            $deserializedBody = $this->serializer->deserialize($request->getContent(), $argument->getType(), 'json');
        } catch (NotNormalizableValueException $e) {
            $vL = new ConstraintViolationList();
            $vL->add((new ConstraintViolation(
                'body couldn\'t be parsed',
                null,
                [],
                'body',
                '',
                $request->getContent()
            )));
            throw new  RequestValidationException($vL, 'NotNormalizableValueException occurred', 0, $e);
        }

        $validationResult = $this->validator->validate($deserializedBody);

        if ($validationResult->count() > 0) {
            throw new RequestValidationException($validationResult, 'Validation failed, please check message');
        }

        yield $deserializedBody;
    }
}
