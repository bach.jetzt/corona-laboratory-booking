<?php

namespace App\DataFixtures;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserTeam;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('admin@admin')
            ->setName('admin')
            ->setRoles(['ROLE_ADMIN']);

        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'adminadmin'));
        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail('fleissiges@bienchen')
            ->setName('fleissiges bienchen');

        $user2->setPassword($this->passwordEncoder->encodePassword(
            $user2,
            'fb'));

        $manager->persist($user2);

        $user3 = new User();
        $user3->setEmail('tman@tman')
            ->setName('tman');

        $user3->setPassword($this->passwordEncoder->encodePassword(
            $user3,
            'tman'));

        $manager->persist($user3);

        $team2 = $manager->getRepository(Team::class)->findAll()[0];

        $userTeam = new UserTeam();
        $userTeam
            ->setUser($user2)
            ->setTeam($team2)
            ->setPermission(UserTeam::PERMISSION_OWNER);
        $manager->persist($userTeam);

        $userTeam2 = new UserTeam();
        $userTeam2
            ->setUser($user3)
            ->setTeam($team2)
            ->setPermission(UserTeam::PERMISSION_USER);
        $manager->persist($userTeam2);

        $manager->flush();
    }
}
