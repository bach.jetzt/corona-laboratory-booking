<?php

namespace App\DataFixtures;

use App\Entity\Room;
use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Uuid;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $team = (new Team())
            ->setName('A-Team')
            ->setInvitationCode(Uuid::v4());
        $manager->persist($team);

        $room = (new Room())
            ->setNumber('A.2a')
            ->setFloorName('Floor A')
            ->setBuilding('A')
            ->setAllowedVisitors(1)
            ->setTeam($team);
        $manager->persist($room);

        $room2 = (new Room())
            ->setNumber('B.2b')
            ->setFloorName('Floor A')
            ->setBuilding('A')
            ->setAllowedVisitors(2)
            ->setTeam($team);
        $manager->persist($room2);

        $team2 = (new Team())
            ->setName('B-Team')
            ->setInvitationCode(Uuid::v4());
        $manager->persist($team2);

        $room3 = (new Room())
            ->setNumber('C.2c')
            ->setFloorName('Floor B')
            ->setBuilding('B')
            ->setAllowedVisitors(1)
            ->setTeam($team2);
        $manager->persist($room3);

        $manager->flush();
    }
}
