<?php

namespace App\Command;

use App\Repository\BookingRepository;
use Monolog\DateTimeImmutable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BookingCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'app:booking:checkout';
    private BookingRepository $bookingRepo;


    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepo = $bookingRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('close bookings.')
            ->setHelp('Closes unfinished bookings for teams with ...')
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'show/closes bookings with forced_checkout_time set'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return Command::SUCCESS;
        }

        if (!$input->getOption('force')) {
            $output->writeln('not implemented yet');

            $table = new Table($output);
            $table->setHeaders(['BookingId', 'Person', 'Room number', 'checkin time', 'checkout time']);
            $bookings = $this->bookingRepo->overdueBookings();
            foreach ($bookings as $booking) {
                $table->addRow([
                    $booking->getId(),
                    $booking->getUser()->getName(),
                    $booking->getRoom()->getNumber(),
                    $booking->getCheckin() === null ? null : $booking->getCheckin()->format(DATE_ATOM),
                    $booking->getCheckout() === null ? null : $booking->getCheckout()->format(DATE_ATOM)
                ]);
            }
            $table->render();

            return Command::SUCCESS;
        }

        $bookings = $this->bookingRepo->overdueBookings();
        $now = new \DateTimeImmutable('now');
        foreach ($bookings as $booking) {
            $now = $now->setTime(
                $booking->getRoom()->getTeam()->getForcedCheckoutTime()->format('H'),
                $booking->getRoom()->getTeam()->getForcedCheckoutTime()->format('i'),
                $booking->getRoom()->getTeam()->getForcedCheckoutTime()->format('s')
            );
            /** @var DateTimeImmutable $checkin */
            $checkin = $booking->getCheckin();
            $now = $now->setDate(
                $checkin->format('Y'),
                $checkin->format('m'),
                $checkin->format('d')
            );
            $output->writeln('working on ' . $booking->getId() . ' ' . $now->format(DATE_ATOM));
            $this->bookingRepo->checkout($booking->getUser(), $booking->getRoom(), $now);
        }
        $output->writeln('done');

        return Command::SUCCESS;
    }
}
