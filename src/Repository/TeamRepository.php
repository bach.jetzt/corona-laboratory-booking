<?php

namespace App\Repository;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserTeam;
use App\Filter\Builders\QueryBuilder;
use App\Filter\PagedResult;
use App\Filter\TeamFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamRepository extends ServiceEntityRepository
{
    private Reader $reader;
    private PropertyInfoExtractorInterface $infoExtractor;
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(
        ManagerRegistry $registry,
        Reader $reader,
        PropertyInfoExtractorInterface $infoExtractor,
        PropertyAccessorInterface $propertyAccessor
    )
    {
        $this->reader = $reader;
        $this->infoExtractor = $infoExtractor;
        $this->propertyAccessor = $propertyAccessor;
        parent::__construct($registry, Team::class);
    }

    public function createTeam(Team $newTeam): Team
    {
        $newTeam->setInvitationCode(Uuid::v4());
        $this->_em->persist($newTeam);
        $this->_em->flush();
        return $newTeam;
    }

    public function updateTeam(Team $updatedTeam): Team
    {
        $this->_em->flush();
        return $updatedTeam;
    }

    public function join(Team $team, User $user): void
    {
        $userTeam = (new UserTeam())
            ->setUser($user)
            ->setTeam($team)
            ->setPermission(UserTeam::PERMISSION_USER);
        $user->addUserTeam($userTeam);
        $this->_em->persist($userTeam);
        $this->_em->flush();
    }

    /**
     * @param User $user
     * @return User[]
     */
    public function search(User $user): array
    {
        $query = $this->_em->createQuery(
            '
            SELECT ut
            FROM App\Entity\UserTeam ut
            WHERE ut.user = :userId
        '
        )->setParameter('userId', $user->getId());

        return $query->getResult();
    }

    public function getFiltered(TeamFilter $filter, string $userId = null): PagedResult
    {
        $queryBuilder = $this->createQueryBuilder('t');

        $queryBuilder = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilder);

        if ($userId !== null) {
            $queryBuilder->innerJoin('t.userTeams', 'ut')
                ->andWhere('ut.user = :userId')
                ->setParameter('userId', $userId);
        }

        $query = $queryBuilder->getQuery();
        $results = $query->getResult();

        $queryBuilderCount = $this->createQueryBuilder('t');
        $queryBuilderCount = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilderCount);

        if ($userId !== null) {
            $queryBuilderCount->innerJoin('t.userTeams', 'ut')
                ->andWhere('ut.user = :userId')
                ->setParameter('userId', $userId);
        }

        $queryBuilderCount
            ->select('COUNT(t.id)')
            ->setFirstResult(null)
            ->setMaxResults(null);

        $totalCount = $queryBuilderCount->getQuery()->getSingleScalarResult();

        return new PagedResult($results, $filter->offset, $filter->limit, $totalCount);
    }
}
