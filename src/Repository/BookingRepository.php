<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\PseudoEntity\PseudoBooking;
use App\Entity\PseudoEntity\PseudoBookingTeam;
use App\Entity\Room;
use App\Entity\User;
use App\Filter\BookingFilter;
use App\Filter\Builders\QueryBuilder;
use App\Filter\PagedResult;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\DBAL\FetchMode;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    private Reader $reader;
    private PropertyInfoExtractorInterface $infoExtractor;
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(
        ManagerRegistry $registry,
        Reader $reader,
        PropertyInfoExtractorInterface $infoExtractor,
        PropertyAccessorInterface $propertyAccessor
    ) {
        $this->reader = $reader;
        $this->infoExtractor = $infoExtractor;
        $this->propertyAccessor = $propertyAccessor;
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param User $user
     * @param string $roomId
     * @return Booking[]
     */
    public function findOpenBookings(User $user, string $roomId): array
    {
        $query = $this->_em->createQuery(
            '
            SELECT b
            FROM App\Entity\Booking b
            WHERE b.room = :roomId AND b.user = :userId AND b.checkout IS NULL'
        )
            ->setParameter('userId', $user->getId())
            ->setParameter('roomId', $roomId);
        return $query->getResult();
    }

    public function checkin(User $user, Room $room, DateTimeImmutable $checkinTime): Booking
    {
        $booking = new Booking();
        $booking->setUser($user)
            ->setRoom($room)
            ->setCheckin($checkinTime);
        $this->_em->persist($booking);
        $this->_em->flush();
        return $booking;
    }

    public function checkout(User $user, Room $room, DateTimeImmutable $checkoutTime): Booking
    {
        $bookings = $this->findOpenBookings($user, $room->getId());
        $booking = reset($bookings);
        $booking->setCheckout($checkoutTime);
        $this->_em->flush();
        return $booking;
    }

    public function getFiltered(User $user, BookingFilter $filter): PagedResult
    {
        $queryBuilder = $this->createQueryBuilder('b');

        $queryBuilder = (new QueryBuilder($this->reader, $this->infoExtractor, $this->propertyAccessor))->build($filter, $queryBuilder);
        $queryBuilder
            ->andWhere('b.user = :userId')
            ->setParameter('userId', $user->getId());

        $queryBuilderCount = $this->createQueryBuilder('b');

        $queryBuilderCount = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilderCount);
        $queryBuilderCount
            ->select('COUNT(b.id)')
            ->andWhere('b.user = :userId')
            ->setParameter('userId', $user->getId())
            ->setFirstResult(null)
            ->setMaxResults(null);
        $totalCount = $queryBuilderCount->getQuery()->getSingleScalarResult();

        $results = $queryBuilder->getQuery()->getResult();
        return new PagedResult($results, $filter->offset, $filter->limit, $totalCount);
    }

    /**
     * @return Booking[]
     */
    public function overdueBookings(): array
    {
//        $sql =
//            '
//            SELECT b.*, t.forced_checkout_time, TIME(NOW())
//            FROM bookings b
//            INNER JOIN rooms r on b.room_id = r.id
//            INNER JOIN teams t on r.team_id = t.id
//            WHERE t.forced_checkout_time IS NOT NULL
//              AND b.checkout IS NULL
//              AND now() < t.forced_checkout_time
//        ';
        $dql = /** @lang DQL */
            '
            SELECT b
            FROM App\Entity\Booking b
            INNER JOIN App\Entity\Room r WITH r.id = b.room
            INNER JOIN App\Entity\Team t WITH t.id = r.team
            WHERE t.forcedCheckoutTime IS NOT NULL 
            AND b.checkout IS NULL 
            AND (CURRENT_TIME() >= t.forcedCheckoutTime OR CURRENT_DATE() > b.checkin) 
        ';

        $query = $this->_em->createQuery($dql);
        return $query->getResult();
    }

    /**
     * @return PseudoBooking[]
     */
    public function encounterReport(User $user, DateTimeImmutable $from, DateTimeImmutable $until): array
    {
        $sql = /** @lang MySQL */
            '
            select 
                   b.*,
                   r.*,
                   group_concat(distinct u2.name) as encounters
            from bookings as b
            INNER JOIN rooms as r on b.room_id = r.id
            left join bookings as bo on bo.room_id = b.room_id and b.checkin < bo.checkout and b.checkout > bo.checkin and bo.user_id != :userId
            left join users as u2 on bo.user_id = u2.id
            where b.user_id = :userId2 
              and b.checkin between :fromDate 
              and :untilDate
            group by b.id
            order by b.checkin asc
            ;';

        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute(
            [
                ':userId' => $user->getId(),
                ':userId2' => $user->getId(),
                ':fromDate' => $from->format('Y-m-d'),
                ':untilDate' => $until->format('Y-m-d')
            ]
        );
        $result = $stmt->fetchAll(FetchMode::STANDARD_OBJECT);

        $encounters = [];

        foreach ($result as $row) {
            $encounter = new PseudoBooking();
            $encounter->roomNumber = $row->number;
            $encounter->floorName = $row->floor_name;
            $encounter->building = $row->building;
            $encounter->checkin = new DateTimeImmutable($row->checkin);
            $encounter->checkout = $row->checkout === null ? null : new DateTimeImmutable($row->checkout);
            $encounter->encounters = \explode(',', $row->encounters);
            $encounters[] = $encounter;
        }

        return $encounters;
    }

    /**
     * @param string $teamId
     * @param DateTimeImmutable $from
     * @param DateTimeImmutable $until
     * @return PseudoBookingTeam[][]
     */
    public function teamReport(string $teamId, DateTimeImmutable $from, DateTimeImmutable $until): array
    {
        $sql = /** @lang MySQL */
            '
            select 
                   b.*,
                   r.*,
                   u.name AS user_name
            from bookings as b
            INNER JOIN rooms as r on b.room_id = r.id
            INNER JOIN users u on b.user_id = u.id
            where r.team_id = :teamId
              and b.checkin between :fromDate 
              and :untilDate
            order by r.building, r.floor_name, r.number, b.checkin asc
            ;';

        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute(
            [
                ':teamId' => $teamId,
                ':fromDate' => $from->format('Y-m-d'),
                ':untilDate' => $until->format('Y-m-d')
            ]
        );
        $result = $stmt->fetchAll(FetchMode::STANDARD_OBJECT);

        $encounters = [];

        foreach ($result as $row) {
            $encounter = new PseudoBookingTeam();

            $key = $row->building . ' - ' . $row->floor_name . ' - ' . $row->number;

            if (!array_key_exists($key, $encounters)) {
                $encounters[$key] = [];
            }

            $encounter->roomNumber = $row->number;
            $encounter->floorName = $row->floor_name;
            $encounter->building = $row->building;
            $encounter->userName = $row->user_name;
            $encounter->checkin = new DateTimeImmutable($row->checkin);
            $encounter->checkout = $row->checkout === null ? null : new DateTimeImmutable($row->checkout);
            $encounters[$key][] = $encounter;
        }

        return $encounters;
    }
}
