<?php

namespace App\Repository;

use App\Entity\Room;
use App\Entity\User;
use App\Entity\UserRoomFavorite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserRoomFavorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRoomFavorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRoomFavorite[]    findAll()
 * @method UserRoomFavorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRoomFavoritesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRoomFavorite::class);
    }

    public function remove(UserRoomFavorite $userRoomFavorite)
    {
        $this->_em->remove($userRoomFavorite);
        $this->_em->flush();
    }

    public function addRoomToFavorites(User $user, Room $room)
    {
        $userRoomFavorite = new UserRoomFavorite();
        $userRoomFavorite->setUser($user);
        $userRoomFavorite->setRoom($room);
        $userRoomFavorite->setOrder(0);
        $this->_em->persist($userRoomFavorite);
        $this->_em->flush();
    }
}
