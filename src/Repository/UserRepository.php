<?php

namespace App\Repository;

use App\Entity\User;
use App\Filter\Builders\QueryBuilder;
use App\Filter\PagedResult;
use App\Filter\UserFilter;
use App\Messages\CreateUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;
    private Reader $reader;
    private PropertyInfoExtractorInterface $infoExtractor;
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(
        ManagerRegistry $registry,
        UserPasswordEncoderInterface $passwordEncoder,
        Reader $reader,
        PropertyInfoExtractorInterface $infoExtractor,
        PropertyAccessorInterface $propertyAccessor
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->reader = $reader;
        $this->infoExtractor = $infoExtractor;
        $this->propertyAccessor = $propertyAccessor;
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function createUser(CreateUser $createUser): User
    {
        $u = (new User())
            ->setEmail($createUser->email)
            ->setName($createUser->name);
        $u->setPassword($this->passwordEncoder->encodePassword($u, $createUser->password));
        $this->_em->persist($u);
        $this->_em->flush();
        return $u;
    }

    public function getFiltered(UserFilter $filter): PagedResult
    {
        $queryBuilder = $this->createQueryBuilder('u');

        $queryBuilder = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilder);
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();

        $queryBuilderCount = $this->createQueryBuilder('u');
        $queryBuilderCount = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilderCount);

        $queryBuilderCount
            ->select('COUNT(u.id)')
            ->setFirstResult(null)
            ->setMaxResults(null);

        $totalCount = $queryBuilderCount->getQuery()->getSingleScalarResult();

        return new PagedResult($results, $filter->offset, $filter->limit, $totalCount);
    }

    public function updatePasswordResetToken(User $user, ?string $token): User
    {
        $user->setPasswordResetToken($token);
        $this->_em->flush();
        return $user;
    }
}
