<?php

namespace App\Repository;

use App\Entity\Room;
use App\Entity\User;
use App\Filter\Builders\QueryBuilder;
use App\Filter\PagedResult;
use App\Filter\RoomFilter;
use App\Filter\TeamFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    private Reader $reader;
    private PropertyInfoExtractorInterface $infoExtractor;
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(
        ManagerRegistry $registry,
        Reader $reader,
        PropertyInfoExtractorInterface $infoExtractor,
        PropertyAccessorInterface $propertyAccessor
    ) {
        $this->reader = $reader;
        $this->infoExtractor = $infoExtractor;
        $this->propertyAccessor = $propertyAccessor;
        parent::__construct($registry, Room::class);
    }

    public function createRoom(Room $newRoom): Room
    {
        $this->_em->persist($newRoom);
        $this->_em->flush();
        return $newRoom;
    }

    public function updateRoom(Room $updatedRoom): Room
    {
        $this->_em->flush();
        return $updatedRoom;
    }

    public function deleteRoom(Room $room): void
    {
        $this->_em->remove($room);
        $this->_em->flush();
    }

    public function getFiltered(User $user, RoomFilter $filter): PagedResult
    {
        $queryBuilder = $this->createQueryBuilder('r');

        $queryBuilder = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilder);
        $queryBuilder
            ->innerJoin('r.team', 't')
            ->innerJoin('t.userTeams', 'ut')
            ->andWhere('ut.user = :userId')
            ->setParameter('userId', $user->getId());
        $query = $queryBuilder->getQuery();
        $results = $query->getResult();

        $queryBuilderCount = $this->createQueryBuilder('r');
        $queryBuilderCount = (new QueryBuilder(
            $this->reader,
            $this->infoExtractor,
            $this->propertyAccessor
        ))->build($filter, $queryBuilderCount);
        $queryBuilderCount
            ->select('COUNT(r.id)')
            ->innerJoin('r.team', 't')
            ->innerJoin('t.userTeams', 'ut')
            ->andWhere('ut.user = :userId')
            ->setParameter('userId', $user->getId())
            ->setFirstResult(null)
            ->setMaxResults(null);

        $totalCount = $queryBuilderCount->getQuery()->getSingleScalarResult();

        return new PagedResult($results, $filter->offset, $filter->limit, $totalCount);
    }
}
