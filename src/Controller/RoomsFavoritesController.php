<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserRoomFavorite;
use App\EventListener\ParameterValidation;
use App\Messages\MyUserResponse;
use App\Repository\RoomRepository;
use App\Repository\UserRoomFavoritesRepository;
use App\Validations\ValidRoom;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class RoomsFavoritesController extends AbstractController
{
    private UserRoomFavoritesRepository $roomFavoritesRepository;
    private RoomRepository $roomRepository;
    private AutoMapperInterface $mapper;

    public function __construct(
        UserRoomFavoritesRepository $roomFavoritesRepository,
        AutoMapperInterface $mapper,
        RoomRepository $roomRepository
    )
    {
        $this->roomFavoritesRepository = $roomFavoritesRepository;
        $this->mapper = $mapper;
        $this->roomRepository = $roomRepository;
    }

    /**
     * @ParameterValidation(property="roomId", constraints={@Assert\Uuid(), @ValidRoom()})
     */
    public function add(string $roomId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $room = $this->roomRepository->find($roomId);

        $this->roomFavoritesRepository->addRoomToFavorites($user, $room);

        return $this->json(
            $this->mapper->map($user, MyUserResponse::class),
            Response::HTTP_OK,
        );
    }

    /**
     * @ParameterValidation(property="roomId", constraints={@Assert\Uuid(), @ValidRoom()})
     */
    public function remove(string $roomId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $urfs = \array_filter($user->getUserRoomFavorites()->getValues(), fn(UserRoomFavorite $urf) => $urf->getRoom()->getId() === $roomId);
        if (\count($urfs) > 0) {
            $this->roomFavoritesRepository->remove(\reset($urfs));
        }

        return $this->json(
            $this->mapper->map($user, MyUserResponse::class),
            Response::HTTP_OK,
        );
    }
}
