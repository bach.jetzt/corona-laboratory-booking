<?php

namespace App\Controller;

use App\Command\BookingCommand;
use App\Entity\Room;
use App\Entity\User;
use App\Filter\BookingFilter;
use App\Messages\BookingResponse;
use App\Messages\Checkin;
use App\Messages\Checkout;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use AutoMapperPlus\AutoMapperInterface;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

class BookingsController extends AbstractController
{
    private AutoMapperInterface $mapper;
    private RoomRepository $roomRepo;
    private BookingRepository $bookingRepo;

    public function __construct(AutoMapperInterface $mapper, RoomRepository $roomRepository, BookingRepository $bookingRepository)
    {
        $this->mapper = $mapper;
        $this->roomRepo = $roomRepository;
        $this->bookingRepo = $bookingRepository;
    }

    public function checkin(Checkin $payload)
    {
        /** @var User $user */
        $user = $this->getUser();

        $bookingTime = $payload->time ?? new DateTimeImmutable('now');

        /** @var Room $room */
        $room = $this->roomRepo->find($payload->roomId);

        $createdBooking = $this->bookingRepo->checkin($user, $room, $bookingTime);

        $mappedData = $this->mapper->map($createdBooking, BookingResponse::class);
        return $this->json($mappedData, Response::HTTP_CREATED);
    }

    public function checkout(Checkout $payload)
    {
        /** @var User $user */
        $user = $this->getUser();

        $bookingTime = $payload->time ?? new DateTimeImmutable('now');

        /** @var Room $room */
        $room = $this->roomRepo->find($payload->roomId);

        $updatedBooking = $this->bookingRepo->checkout($user, $room, $bookingTime);

        $mappedData = $this->mapper->map($updatedBooking, BookingResponse::class);
        return $this->json($mappedData);
    }

    public function getFiltered(BookingFilter $filter)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** PagedResult<Booking[]> */
        $bookingsPagedResult = $this->bookingRepo->getFiltered($user, $filter);
        /** PagedResult<BookingResponse[]> */
        return $bookingsPagedResult->setData(
            $this->mapper->mapMultiple($bookingsPagedResult->getData(), BookingResponse::class)
        );
    }

    public function closeOpenBookings(KernelInterface $kernel)
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(
            [
                'command' => BookingCommand::getDefaultName(),
                // (optional) define the value of command arguments
                '--force' => true
            ]
        );

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        // return new Response(""), if you used NullOutput()
        return new Response($content);
    }
}
