<?php

namespace App\Controller;

use App\Messages\CreateUser;
use App\Messages\LostPassword;
use App\Messages\LostPasswordReset;
use App\Messages\MyUserResponse;
use App\Repository\UserRepository;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;

class SecurityController extends AbstractController
{
    public function login()
    {
    }

    public function logout()
    {
    }

    public function register(CreateUser $payload, UserRepository $userRepository, AutoMapperInterface $mapper): JsonResponse
    {
        $oldUser = $userRepository->findOneBy(['email' => $payload->email]);

        if ($oldUser !== null) {
            return new JsonResponse(['message' => 'email already registered'], Response::HTTP_BAD_REQUEST);
        }
        $newUser = $userRepository->createUser($payload);


        return $this->json($mapper->map($newUser, MyUserResponse::class), Response::HTTP_CREATED);
    }

    public function lostPassword(LostPassword $payload, UserRepository $userRepository, AutoMapperInterface $mapper, MailerInterface $mailer)
    {
        $user = $userRepository->findOneBy(['email' => $payload->username]);

        if ($user !== null) {
            $token = Uuid::v4();
            $userRepository->updatePasswordResetToken($user, $token);
            $appUrl = $this->getParameter('app.password-reset-url');
            // send mail with token-link
            $email = (new Email())
                ->from('Lab Booking App <no-reply@labbooking.app>')
                ->to($user->getEmail())
                ->subject('Password reset request!')
                ->text('You requested a password reset. Copy and paste this link into your browser: ' . $appUrl . $token)
                ->html('<h2>You requested a password reset for LabBooking.App</h2><p>Visit this link to reset your password: <a href="' . $appUrl . $token . '">' . $appUrl . $token . '</a></p>');

            $mailer->send($email);
        }

        return new JsonResponse(['message' => 'email has been sent to ' . $payload->username], Response::HTTP_ACCEPTED);
    }

    public function lostPasswordReset(LostPasswordReset $payload, UserRepository $userRepository, AutoMapperInterface $mapper, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $userRepository->findOneBy(['passwordResetToken' => $payload->resetToken]);

        if ($user !== null) {
            $newPassword = $payload->password;
            $userRepository->upgradePassword($user, $passwordEncoder->encodePassword($user, $newPassword));
            $userRepository->updatePasswordResetToken($user, null);
            return $this->json($mapper->map($user, MyUserResponse::class), Response::HTTP_OK);
        }

        return new Response();
    }

    public function me(AutoMapperInterface $mapper): JsonResponse
    {
        return $this->json($mapper->map($this->getUser(), MyUserResponse::class));
    }
}
