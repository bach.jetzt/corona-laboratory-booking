<?php

namespace App\Controller;

use App\Entity\Room;
use App\Entity\User;
use App\Entity\UserTeam;
use App\EventListener\ParameterValidation;
use App\Filter\PagedResult;
use App\Filter\RoomFilter;
use App\Messages\CreateRoom;
use App\Messages\RoomResponse;
use App\Messages\UpdateRoom;
use App\Repository\RoomRepository;
use App\Validations\ValidRoom;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class RoomsController extends AbstractController
{
    private RoomRepository $roomRepo;
    private AutoMapperInterface $mapper;

    public function __construct(RoomRepository $roomRepository, AutoMapperInterface $mapper)
    {
        $this->roomRepo = $roomRepository;
        $this->mapper = $mapper;
    }

    public function create(CreateRoom $payload): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($payload) {
                return $userTeam->getTeam()->getId() === $payload->teamId && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER;
            }
        );
        if (\count($usersTeam) < 1) {
            return new JsonResponse(['message' => 'you are not participant of the team or you don\t have the OWNER permission']);
        }
        /** @var Room $room */
        $room = $this->mapper->map($payload, Room::class);

        $room->setTeam($usersTeam->first()->getTeam());

        $createdRoom = $this->roomRepo->createRoom($room);

        return $this->json(
            $this->mapper->map($createdRoom, RoomResponse::class),
            Response::HTTP_CREATED,
            ['Location' => $this->generateUrl('app_rooms_byid', ['roomId' => $createdRoom->getId()])]
        );
    }

    /**
     * @ParameterValidation(property="roomId", constraints={@Assert\Uuid(), @ValidRoom()})
     */
    public function update(UpdateRoom $payload, string $roomId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($payload) {
                return $userTeam->getTeam()->getId() === $payload->teamId && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER;
            }
        );
        if (\count($usersTeam) < 1) {
            return new JsonResponse(['message' => 'you are not participant of the team or you don\t have the OWNER permission'], Response::HTTP_FORBIDDEN);
        }

        $oldRoom = $this->roomRepo->find($roomId);

        /** @var Room $room */
        $room = $this->mapper->mapToObject($payload, $oldRoom);
        $updatedRoom = $this->roomRepo->updateRoom($room);
        return $this->json($this->mapper->map($updatedRoom, RoomResponse::class));
    }

    /**
     * @ParameterValidation(property="invitationCode", constraints={@Assert\Uuid(), @ValidRoom()})
     */
    public function delete(string $roomId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Room $oldRoom */
        $oldRoom = $this->roomRepo->find($roomId);

        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($oldRoom) {
                return $userTeam->getTeam()->getId() === $oldRoom->getTeam()->getId()
                    && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER;
            }
        );
        if (\count($usersTeam) < 1) {
            return new JsonResponse(
                ['message' => 'you are not participant of the team or you don\t have the OWNER permission']
            );
        }

        $this->roomRepo->deleteRoom($oldRoom);
        return new JsonResponse(['message' => 'room removed successfully']);
    }

    /**
     * @ParameterValidation(property="roomId", constraints={@Assert\Uuid(), @ValidRoom()})
     */
    public function getById(string $roomId): JsonResponse
    {
        return $this->json($this->mapper->map($this->roomRepo->find($roomId), RoomResponse::class));
    }

    public function getFiltered(RoomFilter $filter): PagedResult
    {
        /** @var User $user */
        $user = $this->getUser();
        /** PagedResult<Room[]> */
        $roomPagedResult = $this->roomRepo->getFiltered($user, $filter);
        /** PagedResult<RoomResponse[]> */
        return $roomPagedResult->setData(
            $this->mapper->mapMultiple($roomPagedResult->getData(), RoomResponse::class)
        );
    }
}
