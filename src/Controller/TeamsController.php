<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserTeam;
use App\EventListener\ParameterValidation;
use App\Filter\PagedResult;
use App\Filter\TeamFilter;
use App\Messages\ChangeUserPermissionMessage;
use App\Messages\CreateTeam;
use App\Messages\MyTeamResponse;
use App\Messages\MyUserResponse;
use App\Messages\TeamResponse;
use App\Messages\UpdateTeam;
use App\Repository\TeamRepository;
use App\Repository\UserRepository;
use App\Repository\UserTeamRepository;
use App\Validations\ValidInvitationCode;
use App\Validations\ValidTeam;
use App\Validations\ValidUser;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use function array_filter;
use function count;

class TeamsController extends AbstractController
{
    private AutoMapperInterface $mapper;
    private TeamRepository $teamRepo;
    private UserTeamRepository $userTeamRepo;


    public function __construct(AutoMapperInterface $mapper, TeamRepository $teamRepository, UserTeamRepository $userTeamRepository)
    {
        $this->mapper = $mapper;
        $this->teamRepo = $teamRepository;
        $this->userTeamRepo = $userTeamRepository;
    }

    /**
     * @ParameterValidation(property="invitationCode", constraints={@Assert\Uuid(), @ValidInvitationCode()})
     */
    public function getInformationByInvitationCode(string $invitationCode): JsonResponse
    {
        /** @var Team $team */
        $team = $this->teamRepo->findOneBy(['invitationCode' => $invitationCode]);
        return $this->json($this->mapper->map($team, TeamResponse::class));
    }

    /**
     * @ParameterValidation(property="invitationCode", constraints={@Assert\Uuid(), @ValidInvitationCode()})
     */
    public function join(string $invitationCode): JsonResponse
    {
        //TODO user is already member of this team (validation)
        /** @var Team $team */
        $team = $this->teamRepo->findOneBy(['invitationCode' => $invitationCode]);
        /** @var User $user */
        $user = $this->getUser();
        $this->teamRepo->join($team, $user);

        return $this->json($this->mapper->map($user, MyUserResponse::class));
    }

    public function getTeams(TeamFilter $filter): PagedResult
    {
        /** @var User $user */
        $user = $this->getUser();
        $userId = null;
        if (!$this->isGranted('ROLE_ADMIN')) {
            $userId = $user->getId();
        }

        $userTeamPageResult = $this->teamRepo->getFiltered($filter, $userId);
        return $userTeamPageResult->setData($this->mapper->mapMultiple($userTeamPageResult->getData(), MyTeamResponse::class));
    }

    public function create(CreateTeam $payload): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $newTeam = $this->mapper->map($payload, Team::class);
        $createdTeam = $this->teamRepo->createTeam($newTeam);
        return $this->json($this->mapper->map($createdTeam, TeamResponse::class));
    }

    /**
     * @ParameterValidation(property="teamId", constraints={@Assert\Uuid(), @ValidTeam()})
     */
    public function update(UpdateTeam $payload, string $teamId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($teamId) {
                return $userTeam->getTeam()->getId() === $teamId
                    && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER;
            }
        );
        $userTeamCount = $usersTeam->count();
        $isAdmin = $this->isGranted('ROLE_ADMIN');
        if (!$isAdmin && $userTeamCount < 1) {
            return new JsonResponse(
                ['message' => 'you are not participant of the team or you don\t have the OWNER permission'],
                Response::HTTP_FORBIDDEN
            );
        }
        /** @var Team $oldTeam */
        $oldTeam = $this->teamRepo->find($teamId);

        if ($payload->name !== $oldTeam->getName()) {
            $oldTeam->setInvitationCode(Uuid::v4());
        }

        $team = $this->mapper->mapToObject($payload, $oldTeam);
        $updatedTeam = $this->teamRepo->updateTeam($team);
        return $this->json($this->mapper->map($updatedTeam, MyTeamResponse::class));
    }

    /**
     * @ParameterValidation(property="teamId", constraints={@Assert\Uuid(), @ValidTeam()})
     */
    public function renewToken(string $teamId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($teamId) {
                return $userTeam->getTeam()->getId() === $teamId;
            }
        );

        $isAdmin = $this->isGranted('ROLE_ADMIN');
        if (!$isAdmin && $usersTeam->count() < 1) {
            return new JsonResponse(
                ['message' => 'you are not participant of the team'],
                Response::HTTP_FORBIDDEN
            );
        }
        /** @var Team $oldTeam */
        $oldTeam = $this->teamRepo->find($teamId);

        $oldTeam->setInvitationCode(Uuid::v4());
        $updatedTeam = $this->teamRepo->updateTeam($oldTeam);

        return $this->json($this->mapper->map($updatedTeam, MyTeamResponse::class));
    }

    /**
     * @ParameterValidation(property="teamId", constraints={@Assert\Uuid(), @ValidTeam()})
     * @ParameterValidation(property="userId", constraints={@Assert\Uuid(), @ValidUser()})
     */
    public function removeUser(string $teamId, string $userId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($teamId) {
                return $userTeam->getTeam()->getId() === $teamId;
            }
        )->getValues();

        $isAdmin = $this->isGranted('ROLE_ADMIN');
        if (!$isAdmin && count(array_filter($usersTeam, fn(UserTeam $ut) => $ut->getPermission() === 'OWNER')) < 1) {
            return new JsonResponse(
                ['message' => 'you are not an owner of the team'],
                Response::HTTP_FORBIDDEN
            );
        }
        /** @var Team $oldTeam */
        $oldTeam = $this->teamRepo->find($teamId);
        $ut = $oldTeam->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($userId) {
                return $userTeam->getUser()->getId() === $userId;
            }
        );
        $this->userTeamRepo->removeEntry($ut->first());
        $updatedTeam = $this->teamRepo->find($oldTeam->getId());

        return $this->json($this->mapper->map($updatedTeam, MyTeamResponse::class));
    }

    /**
     * @ParameterValidation(property="teamId", constraints={@Assert\Uuid(), @ValidTeam()})
     */
    public function changeUserPermission(ChangeUserPermissionMessage $payload, string $teamId, UserRepository $userRepository, UserTeamRepository $userTeamRepository)
    {
        /** @var User $user */
        $user = $this->getUser();

        $usersTeam = $user->getUserTeams()->filter(
            fn(UserTeam $userTeam) => $userTeam->getTeam()->getId() === $teamId && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER
        );


        if (\count($usersTeam) < 1) {
            return new JsonResponse(['message' => 'you are not participant of the team or you don\t have the OWNER permission'], Response::HTTP_BAD_REQUEST);
        }

        $userToUpdate = $userRepository->find($payload->userId);

        $usersTeamForUserToUpdate = $userToUpdate->getUserTeams()->filter(
            fn(UserTeam $userTeam) => $userTeam->getTeam()->getId() === $teamId
        );

        if ($usersTeamForUserToUpdate->count() !== 1) {
            return new JsonResponse(['message' => 'user is not part of this team'], Response::HTTP_BAD_REQUEST);
        }

        /** @var UserTeam $userToUpdateUserTeam */
        $userToUpdateUserTeam = $usersTeamForUserToUpdate->first();

        if ($userToUpdateUserTeam->getPermission() !== $payload->newPermission) {
            $userToUpdateUserTeam->setPermission($payload->newPermission);
            $userTeamRepository->update($userToUpdateUserTeam);
        }

        return $this->json($this->mapper->map($userToUpdateUserTeam->getTeam(), MyTeamResponse::class));
    }
}
