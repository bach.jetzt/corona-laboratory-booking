<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    public function index(): Response
    {
        if ($this->getParameter('kernel.environment') !== 'prod') {
            return new Response('', Response::HTTP_NOT_FOUND);
        }
        $index_html_path = $this->getParameter('kernel.project_dir') . '/public/index.html';
        return new Response(file_get_contents($index_html_path));
    }
}
