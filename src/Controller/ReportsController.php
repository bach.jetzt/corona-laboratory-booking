<?php

namespace App\Controller;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\UserTeam;
use App\EventListener\ParameterValidation;
use App\Repository\BookingRepository;
use App\Repository\TeamRepository;
use App\Service\Pdf\MyPdf;
use App\Validations\ValidTeam;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class ReportsController extends AbstractController
{
    private BookingRepository $bookingRepo;
    private TeamRepository $teamRepo;

    public function __construct(BookingRepository $bookingRepository, TeamRepository $teamRepository)
    {
        $this->bookingRepo = $bookingRepository;
        $this->teamRepo = $teamRepository;
    }

    public function personalPdf(Request $request)
    {
        $from = $request->query->get('from', null);
        $until = $request->query->get('until', null);

        if ($from === null || $until === null) {
            return new Response('either from or until or both are missing', Response::HTTP_BAD_REQUEST);
        }

        $from = new DateTimeImmutable($from);
        $until = new DateTimeImmutable($until);

        /** @var User $user */
        $user = $this->getUser();
        $data = $this->bookingRepo->encounterReport($user, $from, $until);

        $pdf = new MyPdf('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($user->getName());
        $pdf->SetTitle('Bookings');
        $pdf->SetSubject('Personal Bookings');
        $pdf->SetKeywords('corona, bookings');

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', 'BI', 12);

        // add a page
        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 18);

        $pdf->Ln(5);
        $textWidth = $pdf->getPageWidth() - $pdf->getMargins()['left'] - $pdf->getMargins()['right'];

        $fromString = $from->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y');
        $untilString = $until->setTimezone(new \DateTimeZone('Europe/Berlin'))->sub(new \DateInterval('PT1S'))->format('d.m.Y');
        $half = $textWidth / 2;
        $pdf->MultiCell($half, 15, $user->getName(), 0, 'L', 0, 0, '', '', true);
        $pdf->MultiCell($half, 15, $fromString . ' - ' . $untilString, 0, 'R', 0, 1, '', '', true);

        // print colored tablew yar
        $pdf->ColoredTable(['Room', 'Floor', 'Building', 'Checkin', 'Checkout', 'Encounters'], $data);

        return new BinaryFileResponse($pdf->Output('personalbookings_' . str_replace('.', '', $fromString) . '_' . str_replace('.', '', $untilString) . '.pdf', 'I'));
    }

    /**
     * @ParameterValidation(property="teamId", constraints={@Assert\Uuid(), @ValidTeam()})
     */
    public function teamsPdf(Request $request, string $teamId)
    {
        $from = $request->query->get('from', null);
        $until = $request->query->get('until', null);

        if ($from === null || $until === null) {
            return new Response('either from or until or both are missing', Response::HTTP_BAD_REQUEST);
        }

        $from = new DateTimeImmutable($from);
        $until = new DateTimeImmutable($until);

        /** @var User $user */
        $user = $this->getUser();

        $usersTeam = $user->getUserTeams()->filter(
            function (UserTeam $userTeam) use ($teamId) {
                return $userTeam->getTeam()->getId() === $teamId
                    && $userTeam->getPermission() === UserTeam::PERMISSION_OWNER;
            }
        );
        $userTeamCount = $usersTeam->count();
        if ($userTeamCount < 1) {
            return new JsonResponse(
                ['message' => 'you are not participant of the team or you don\t have the OWNER permission'],
                Response::HTTP_FORBIDDEN
            );
        }

        $data = $this->bookingRepo->teamReport($teamId, $from, $until);

        /** @var Team $team */
        $team = $this->teamRepo->find($teamId);

        $pdf = new MyPdf('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($user->getName());
        $pdf->SetTitle('Bookings');
        $pdf->SetSubject('Personal Bookings');
        $pdf->SetKeywords('corona, bookings');

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', 'BI', 12);
        // add a page
        $pdf->AddPage();

        $pdf->SetFont('helvetica', 'B', 18);

        $pdf->Ln(5);
        $textWidth = $pdf->getPageWidth() - $pdf->getMargins()['left'] - $pdf->getMargins()['right'];

        $fromString = $from->setTimezone(new \DateTimeZone('Europe/Berlin'))->format('d.m.Y');
        $untilString = $until->setTimezone(new \DateTimeZone('Europe/Berlin'))->sub(new \DateInterval('PT1S'))->format('d.m.Y');

        $pdf->SetFont('helvetica', 'B', 25);
        $pdf->Ln(30);
        $pdf->MultiCell($textWidth, 25, 'Booking Report', 0, 'C', 0, 1, '', '', true);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->MultiCell($textWidth, 15, $team->getName(), 0, 'C', 0, 1, '', '', true);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->MultiCell($textWidth, 15, $fromString . ' - ' . $untilString, 0, 'C', 0, 1, '', '', true);
        $pdf->SetFont('helvetica', 'B', 18);

        $pdf->teamTable($data, $fromString, $untilString);

        return new BinaryFileResponse($pdf->Output('teambookings_' . str_replace('.', '', $fromString) . '_' . str_replace('.', '', $untilString) . '.pdf', 'I'));
    }
}
