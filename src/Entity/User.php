<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="json")
     * @var string[]
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @var string|null A token for resetting the users password via email
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $passwordResetToken;

    /**
     * @ORM\Column(type="string", length=50, unique=false)
     */
    private string $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserTeam", mappedBy="user")
     * @var UserTeam[]
     */
    private $userTeams;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRoomFavorite", mappedBy="user")
     * @var UserRoomFavorite[]
     */
    private $userRoomFavorites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking",mappedBy="user")
     */
    private $bookings;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->userTeams = new ArrayCollection();
        $this->userRoomFavorites = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|UserTeam[]
     */
    public function getUserTeams(): Collection
    {
        return $this->userTeams;
    }

    public function addUserTeam(UserTeam $userTeam): self
    {
        if (!$this->userTeams->contains($userTeam)) {
            $this->userTeams[] = $userTeam;
            $userTeam->setUser($this);
        }

        return $this;
    }

    public function removeUserTeam(UserTeam $userTeam): self
    {
        if ($this->userTeams->contains($userTeam)) {
            $this->userTeams->removeElement($userTeam);
            // set the owning side to null (unless already changed)
            if ($userTeam->getUser() === $this) {
                $userTeam->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setUser($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getUser() === $this) {
                $booking->setUser(null);
            }
        }

        return $this;
    }

    public function getPasswordResetToken(): ?string
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?string $passwordResetToken): self
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * @return Collection|UserRoomFavorite[]
     */
    public function getUserRoomFavorites(): Collection
    {
        return $this->userRoomFavorites;
    }

    public function addUserRoomFavorite(UserRoomFavorite $userRoomFavorite): self
    {
        if (!$this->userRoomFavorites->contains($userRoomFavorite)) {
            $this->userRoomFavorites[] = $userRoomFavorite;
            $userRoomFavorite->setUser($this);
        }

        return $this;
    }

    public function removeUserRoomFavorite(UserRoomFavorite $userRoomFavorite): self
    {
        if ($this->userRoomFavorites->contains($userRoomFavorite)) {
            $this->userRoomFavorites->removeElement($userRoomFavorite);
            // set the owning side to null (unless already changed)
            if ($userRoomFavorite->getUser() === $this) {
                $userRoomFavorite->setUser(null);
            }
        }

        return $this;
    }
}
