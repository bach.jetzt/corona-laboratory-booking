<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoomRepository::class)
 * @ORM\Table(name="rooms")
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private string $number;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $floorName;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private string $building;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    private int $allowedVisitors;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="rooms")
     */
    private Team $team;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="room")
     * @ORM\JoinColumn(name="bookings_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $bookings;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getFloorName(): string
    {
        return $this->floorName;
    }

    public function setFloorName(string $floorName): self
    {
        $this->floorName = $floorName;

        return $this;
    }

    public function getAllowedVisitors(): int
    {
        return $this->allowedVisitors;
    }

    public function setAllowedVisitors(int $allowedVisitors): self
    {
        $this->allowedVisitors = $allowedVisitors;

        return $this;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }

    public function setTeam(Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setRoom($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getRoom() === $this) {
                $booking->setRoom(null);
            }
        }

        return $this;
    }

    public function getBuilding(): string
    {
        return $this->building;
    }

    public function setBuilding(string $building): self
    {
        $this->building = $building;

        return $this;
    }
}
