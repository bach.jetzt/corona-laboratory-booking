<?php

namespace App\Entity\PseudoEntity;

use DateTimeImmutable;

class PseudoBooking
{
    public string $roomNumber;
    public string $floorName;
    public string $building;
    public DateTimeImmutable $checkin;
    public ?DateTimeImmutable $checkout;
    /**
     * @var string[]
     */
    public array $encounters;
}
