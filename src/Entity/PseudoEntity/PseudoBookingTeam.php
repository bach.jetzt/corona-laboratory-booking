<?php

namespace App\Entity\PseudoEntity;

use DateTimeImmutable;

class PseudoBookingTeam
{
    public string $roomNumber;
    public string $floorName;
    public string $building;
    public string $userName;
    public DateTimeImmutable $checkin;
    public ?DateTimeImmutable $checkout;
}
