<?php

namespace App\Entity;

use App\Repository\UserRoomFavoritesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=UserRoomFavoritesRepository::class)
 * @ORM\Table(name="users_rooms_favorites", uniqueConstraints={@UniqueConstraint(name="userroomfavorite", columns={"user_id", "room_id"})})
 */
class UserRoomFavorite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userRoomFavorites")
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room")
     */
    private Room $room;

    /**
     * @ORM\Column(type="smallint", nullable=false, options={"unsigned":true, "default":0}, name="`order`")
     */
    private int $order;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoom(): Room
    {
        return $this->room;
    }

    public function setRoom(Room $room): self
    {
        $this->room = $room;

        return $this;
    }
}
