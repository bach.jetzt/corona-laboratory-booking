<?php

namespace App\Entity;

use App\Repository\UserTeamRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=UserTeamRepository::class)
 * @ORM\Table(name="users_teams", uniqueConstraints={@UniqueConstraint(name="userteam", columns={"user_id", "team_id"})})
 */
// ,
class UserTeam
{
    public const PERMISSION_USER = 'USER';
    public const PERMISSION_OWNER = 'OWNER';

    public const PERMISSIONS = [
        self::PERMISSION_OWNER,
        self::PERMISSION_USER
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userTeams")
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="userTeams")
     */
    private Team $team;

    /**
     * @ORM\Column(type="string", nullable=false, options={"USER", "OWNER"})
     */
    private ?string $permission;


    public function getId(): string
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }

    public function setTeam(Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getPermission(): ?string
    {
        return $this->permission;
    }

    public function setPermission(string $permission): self
    {
        $this->permission = $permission;

        return $this;
    }
}
