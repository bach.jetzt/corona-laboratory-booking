<?php

namespace App\Entity;

use App\Repository\TeamRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 * @ORM\Table(name="teams")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="guid", nullable=false)
     */
    private string $invitationCode;

    /**
     * @ORM\Column(type="time_immutable", nullable=true)
     */
    private ?DateTimeImmutable $forcedCheckoutTime;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Room", mappedBy="team")
     */
    private $rooms;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserTeam", mappedBy="team")
     */
    private $userTeams;

    public function __construct()
    {
        $this->rooms = new ArrayCollection();
        $this->userTeams = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInvitationCode(): ?string
    {
        return $this->invitationCode;
    }

    public function setInvitationCode(string $invitationCode): self
    {
        $this->invitationCode = $invitationCode;

        return $this;
    }

    /**
     * @return Collection|Room[]
     */
    public function getRooms(): Collection
    {
        return $this->rooms;
    }

    public function addRoom(Room $room): self
    {
        if (!$this->rooms->contains($room)) {
            $this->rooms[] = $room;
            $room->setTeam($this);
        }

        return $this;
    }

    public function removeRoom(Room $room): self
    {
        if ($this->rooms->contains($room)) {
            $this->rooms->removeElement($room);
            // set the owning side to null (unless already changed)
            if ($room->getTeam() === $this) {
                $room->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserTeam[]
     */
    public function getUserTeams(): Collection
    {
        return $this->userTeams;
    }

    public function addUserTeam(UserTeam $userTeam): self
    {
        if (!$this->userTeams->contains($userTeam)) {
            $this->userTeams[] = $userTeam;
            $userTeam->setTeam($this);
        }

        return $this;
    }

    public function removeUserTeam(UserTeam $userTeam): self
    {
        if ($this->userTeams->contains($userTeam)) {
            $this->userTeams->removeElement($userTeam);
            // set the owning side to null (unless already changed)
//            if ($userTeam->getTeam() === $this) {
//                $userTeam->setTeam(null);
//            }
        }

        return $this;
    }

    public function getForcedCheckoutTime(): ?\DateTimeImmutable
    {
        return $this->forcedCheckoutTime;
    }

    public function setForcedCheckoutTime(?\DateTimeImmutable $forcedCheckoutTime): self
    {
        $this->forcedCheckoutTime = $forcedCheckoutTime;

        return $this;
    }
}
