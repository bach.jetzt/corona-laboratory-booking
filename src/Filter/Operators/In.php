<?php

namespace App\Filter\Operators;

class In extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'in';

    public function __construct(array $value)
    {
        parent::__construct($value);
    }

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
