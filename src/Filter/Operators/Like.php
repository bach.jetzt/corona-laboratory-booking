<?php

namespace App\Filter\Operators;

class Like extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'like';

    public function __construct($value)
    {
        $value = trim($value, '%');
        $value = '%' . $value . '%';
        parent::__construct($value);
    }


    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
