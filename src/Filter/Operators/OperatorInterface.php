<?php

namespace App\Filter\Operators;

interface OperatorInterface
{
    public function getOperator(): string;

    public function inputBindingNeeded(): bool;

    /**
     * @return mixed
     */
    public function getValue();

    public function getUid(): string;

    public function getUidParameter(): string;
}
