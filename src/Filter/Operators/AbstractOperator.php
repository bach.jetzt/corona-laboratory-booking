<?php

namespace App\Filter\Operators;

abstract class AbstractOperator
{
    private const UID_PREFIX = 'clb';

    protected $value;
    protected string $uid;

    /**
     * @param string $field
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
        $this->createUid();
    }

    public function getValue()
    {
        return $this->value;
    }

    private function createUid()
    {
        $this->uid = \str_replace('.', '', uniqid(self::UID_PREFIX, true));
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getUidParameter(): string
    {
        return ':' . $this->getUid();
    }

    abstract public function getOperator(): string;

    abstract public function inputBindingNeeded(): bool;
}
