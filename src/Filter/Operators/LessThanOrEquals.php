<?php

namespace App\Filter\Operators;

class LessThanOrEquals extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'lte';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
