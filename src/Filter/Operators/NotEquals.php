<?php

namespace App\Filter\Operators;

class NotEquals extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'neq';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
