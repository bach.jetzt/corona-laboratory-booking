<?php

namespace App\Filter\Operators;

class IsNull extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'null';

    public function __construct()
    {
        parent::__construct('1');
    }

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return false;
    }
}
