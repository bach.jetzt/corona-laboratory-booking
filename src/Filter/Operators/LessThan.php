<?php

namespace App\Filter\Operators;

class LessThan extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'lt';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
