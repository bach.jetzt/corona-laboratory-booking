<?php

namespace App\Filter\Operators;

class NotIn extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'nin';

    public function __construct(string $field, array $value)
    {
        parent::__construct($field, $value);
    }

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
