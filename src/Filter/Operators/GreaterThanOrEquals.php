<?php

namespace App\Filter\Operators;

class GreaterThanOrEquals extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'gte';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
