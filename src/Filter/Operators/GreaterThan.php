<?php

namespace App\Filter\Operators;

class GreaterThan extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'gt';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
