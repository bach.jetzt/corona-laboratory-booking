<?php

namespace App\Filter\Operators;

class Equals extends AbstractOperator implements OperatorInterface
{
    const OPERATOR = 'eq';

    /**
     * @inheritdoc
     */
    public function getOperator(): string
    {
        return self::OPERATOR;
    }

    public function inputBindingNeeded(): bool
    {
        return true;
    }
}
