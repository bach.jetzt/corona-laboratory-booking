<?php

namespace App\Filter;

use App\Filter\Annotations as Filter;
use App\Filter\Operators\Equals;
use App\Filter\Operators\Like;

class UserFilter extends AbstractFilter
{
    /**
     * @Filter\DbPropertyMapping({"u.id"})
     * @Filter\AllowedOperators({Equals::OPERATOR})
     */
    public ?array $id = null;

    /**
     * @Filter\DbPropertyMapping({"u.email"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"u.email"})
     */
    public ?array $email = null;
}
