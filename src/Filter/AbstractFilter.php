<?php

namespace App\Filter;

use App\Filter\Sorting\SortingInterface;

abstract class AbstractFilter
{
    /** @var SortingInterface[] $sortings */
    public array $sortings = [];

    public int $limit = 20;
    public int $offset = 0;

    /**
     * @return SortingInterface[]
     */
    public function getSortings(): array
    {
        return $this->sortings;
    }

    /**
     * @param SortingInterface[] $sortings
     * @return $this
     */
    public function setSortings(array $sortings): self
    {
        $this->sortings = $sortings;
        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return $this
     */
    public function addSorting(SortingInterface $sorting): self
    {
        $this->sortings[] = $sorting;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }
}
