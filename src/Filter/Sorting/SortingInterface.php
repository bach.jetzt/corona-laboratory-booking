<?php

namespace App\Filter\Sorting;

interface SortingInterface
{
    /**
     * @return string
     */
    public function getField();

    /**
     * @return string
     */
    public function getDirection();
}
