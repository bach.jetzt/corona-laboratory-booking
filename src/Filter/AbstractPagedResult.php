<?php

namespace App\Filter;

abstract class AbstractPagedResult
{
    public int $offset;
    public int $limit;
    public int $totalCount;
    /**
     * @var mixed
     */
    protected $data;

    public function __construct($data, int $offset, int $limit, int $totalCount)
    {
        $this->data = $data;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->totalCount = $totalCount;
    }

    public function getCount(): int
    {
        return count($this->getData());
    }

    abstract function getData(): array;

    abstract function setData(array $data): self;
}
