<?php

namespace App\Filter;

use App\Filter\Annotations as Filter;
use App\Filter\Operators\Equals;
use App\Filter\Operators\GreaterThan;
use App\Filter\Operators\GreaterThanOrEquals;
use App\Filter\Operators\LessThan;
use App\Filter\Operators\LessThanOrEquals;
use App\Filter\Operators\In;
use App\Filter\Operators\IsNull;
use App\Filter\Operators\IsNotNull;

class BookingFilter extends AbstractFilter
{
    /**
     *
     * @Filter\DbPropertyMapping({"b.id"})
     * @Filter\AllowedOperators({Equals::OPERATOR, In::OPERATOR})
     */
    public ?array $id = null;

    /**
     * @Filter\DbPropertyMapping({"b.room"})
     * @Filter\AllowedOperators({Equals::OPERATOR, In::OPERATOR})
     */
    public ?array $roomId = null;

    /**
     * @Filter\DbPropertyMapping({"b.user"})
     * @Filter\AllowedOperators({Equals::OPERATOR, In::OPERATOR})
     */
    public ?array $userId = null;

    /**
     * @Filter\DbPropertyMapping({"b.checkin"})
     * @Filter\AllowedOperators({Equals::OPERATOR, LessThan::OPERATOR, LessThanOrEquals::OPERATOR, GreaterThan::OPERATOR, GreaterThanOrEquals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"b.checkin"})
     */
    public ?array $checkin = null;

    /**
     * @Filter\DbPropertyMapping({"b.checkout"})
     * @Filter\AllowedOperators({Equals::OPERATOR, LessThan::OPERATOR, LessThanOrEquals::OPERATOR, GreaterThan::OPERATOR, GreaterThanOrEquals::OPERATOR, IsNull::OPERATOR, IsNotNull::OPERATOR})
     * @Filter\SortDbPropertyMapping({"b.checkout"})
     */
    public ?array $checkout = null;
}
