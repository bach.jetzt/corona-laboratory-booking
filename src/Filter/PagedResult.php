<?php

namespace App\Filter;

final class PagedResult extends AbstractPagedResult
{
    function getData(): array
    {
        return $this->data;
    }

    function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }
}
