<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\In;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class InOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return In::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->in($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
