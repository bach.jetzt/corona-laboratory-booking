<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\IsNotNull;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class IsNotNullOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return IsNotNull::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field) {
                return $builder->expr()->isNotNull($field);
            }
        );

        return $queryBuilder;
    }
}
