<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\GreaterThan;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class GreaterThanOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return GreaterThan::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->gt($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
