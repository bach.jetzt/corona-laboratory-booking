<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\NotIn;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class NotInOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return NotIn::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->notIn($field, $operator->getUidParameter());
            }
        );
        return $queryBuilder;
    }
}
