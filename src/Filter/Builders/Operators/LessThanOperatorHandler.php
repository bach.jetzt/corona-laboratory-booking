<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\LessThan;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class LessThanOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return LessThan::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->lt($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
