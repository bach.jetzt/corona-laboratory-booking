<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractOperatorHandler implements OperatorHandlerInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param string[] $fields
     * @param OperatorInterface $operator
     * @param callable $relation function (QueryBuilder $builder, $field, OperatorInterface $operator)
     */
    protected function add(QueryBuilder $queryBuilder, array $fields, OperatorInterface $operator, callable $relation): void
    {
        $expList = [];

        foreach ($fields as $field) {
            $expList[] = $relation($queryBuilder, $field, $operator);
        }
        $or = new Expr\Orx($expList);
        $queryBuilder->andWhere($or);

        if ($operator->inputBindingNeeded()) {
            $queryBuilder->setParameter($operator->getUid(), $operator->getValue());
        }
    }
}
