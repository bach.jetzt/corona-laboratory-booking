<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\Like;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class LikeOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return Like::OPERATOR;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string|string[] $fields
     * @param OperatorInterface $operator
     * @return QueryBuilder
     */
    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->like($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
