<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\NotEquals;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class NotEqualsOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return NotEquals::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->neq($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
