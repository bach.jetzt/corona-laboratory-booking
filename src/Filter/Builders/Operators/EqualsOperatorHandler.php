<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\Equals;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class EqualsOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return Equals::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->eq($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
