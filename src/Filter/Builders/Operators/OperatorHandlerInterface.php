<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

interface OperatorHandlerInterface
{
    public function handles(): string;

    /**
     * @param QueryBuilder $queryBuilder
     * @param string[] $fields
     * @param OperatorInterface $operator
     * @return QueryBuilder
     */
    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, array $fields, OperatorInterface $operator): QueryBuilder;
}
