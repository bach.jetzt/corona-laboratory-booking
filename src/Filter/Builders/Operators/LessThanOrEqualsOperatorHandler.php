<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\LessThanOrEquals;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class LessThanOrEqualsOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return LessThanOrEquals::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->lte($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
