<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\IsNull;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class IsNullOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return IsNull::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field) {
                return $builder->expr()->isNull($field);
            }
        );

        return $queryBuilder;
    }
}
