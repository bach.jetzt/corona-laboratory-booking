<?php

namespace App\Filter\Builders\Operators;

use App\Filter\Operators\GreaterThanOrEquals;
use App\Filter\Operators\OperatorInterface;
use Doctrine\ORM\QueryBuilder;

class GreaterThanOrEqualsOperatorHandler extends AbstractOperatorHandler implements OperatorHandlerInterface
{
    public function handles(): string
    {
        return GreaterThanOrEquals::OPERATOR;
    }

    public function addFilterToQueryBuilder(QueryBuilder $queryBuilder, $fields, OperatorInterface $operator): QueryBuilder
    {
        $this->add(
            $queryBuilder,
            $fields,
            $operator,
            function (QueryBuilder $builder, $field, OperatorInterface $operator) {
                return $builder->expr()->gte($field, $operator->getUidParameter());
            }
        );

        return $queryBuilder;
    }
}
