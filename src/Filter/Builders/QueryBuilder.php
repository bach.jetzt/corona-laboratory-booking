<?php

namespace App\Filter\Builders;

use App\Filter\AbstractFilter;
use App\Filter\Annotations\AllowedOperators;
use App\Filter\Annotations\DbPropertyMapping;
use App\Filter\Annotations\SortDbPropertyMapping;
use App\Filter\Builders\Operators\EqualsOperatorHandler;
use App\Filter\Builders\Operators\GreaterThanOperatorHandler;
use App\Filter\Builders\Operators\GreaterThanOrEqualsOperatorHandler;
use App\Filter\Builders\Operators\InOperatorHandler;
use App\Filter\Builders\Operators\IsNotNullOperatorHandler;
use App\Filter\Builders\Operators\IsNullOperatorHandler;
use App\Filter\Builders\Operators\LessThanOperatorHandler;
use App\Filter\Builders\Operators\LessThanOrEqualsOperatorHandler;
use App\Filter\Builders\Operators\LikeOperatorHandler;
use App\Filter\Builders\Operators\NotEqualsOperatorHandler;
use App\Filter\Builders\Operators\NotInOperatorHandler;
use App\Filter\Builders\Operators\OperatorHandlerInterface;
use App\Filter\Operators\OperatorInterface;
use App\Filter\Sorting\Ascending;
use App\Filter\Sorting\Descending;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\QueryBuilder as DoctrineQueryBuilder;
use ReflectionProperty;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use function get_class;

class QueryBuilder
{
    /** @var OperatorHandlerInterface[] */
    protected array $operatorHandlers;

    private Reader $reader;
    private PropertyInfoExtractorInterface $infoExtractor;
    private PropertyAccessorInterface $propertyAccessor;

    /**
     * @param PropertyInfoExtractorInterface $infoExtractor
     * @param PropertyAccessorInterface $propertyAccessor
     * @param OperatorHandlerInterface[] $additionalOperatorHandlers
     */
    public function __construct(Reader $reader, PropertyInfoExtractorInterface $infoExtractor, PropertyAccessorInterface $propertyAccessor, array $additionalOperatorHandlers = [])
    {
        $this->reader = $reader;
        $this->infoExtractor = $infoExtractor;
        $this->propertyAccessor = $propertyAccessor;

        $defaultOperatorHandlers = [
            new EqualsOperatorHandler(),
            new GreaterThanOperatorHandler(),
            new GreaterThanOrEqualsOperatorHandler(),
            new InOperatorHandler(),
            new LessThanOperatorHandler(),
            new LessThanOrEqualsOperatorHandler(),
            new IsNotNullOperatorHandler(),
            new IsNullOperatorHandler(),
            new LikeOperatorHandler(),
            new NotEqualsOperatorHandler(),
            new NotInOperatorHandler(),
        ];

        /** @var OperatorHandlerInterface[] $foperatorHandlers */
        $foperatorHandlers = array_merge($defaultOperatorHandlers, $additionalOperatorHandlers);

        foreach ($foperatorHandlers as $operatorHandler) {
            $this->operatorHandlers[$operatorHandler->handles()] = $operatorHandler;
        }
    }

    /**
     * @param AbstractFilter $filter
     * @param DoctrineQueryBuilder|null $doctrineQueryBuilder
     * @return DoctrineQueryBuilder
     */
    public function build(AbstractFilter $filter, DoctrineQueryBuilder $doctrineQueryBuilder = null): DoctrineQueryBuilder
    {
        $filterProperties = $this->infoExtractor->getProperties(get_class($filter));
        $filterProperties = array_filter($filterProperties, fn(string $name) => !in_array($name, ['sortings', 'limit', 'offset']));

        foreach ($filterProperties as $f) {
            $reflectionProperty = new ReflectionProperty(get_class($filter), $f);
            $dbPropertyAnnotation = $this->reader->getPropertyAnnotation($reflectionProperty, DbPropertyMapping::class);
            $allowedOperatorsAnnotation = $this->reader->getPropertyAnnotation($reflectionProperty, AllowedOperators::class);

            if($dbPropertyAnnotation === null || $allowedOperatorsAnnotation === null){
                continue;
            }

            /** @var OperatorInterface[]|null $filterProperty */
            $filterProperty = $this->propertyAccessor->getValue($filter, $f);

            if ($filterProperty === null) {
                continue;
            }
            foreach ($filterProperty as $operatorInterface) {
                $expr = null;
                if (!in_array($operatorInterface->getOperator(), $allowedOperatorsAnnotation->getValues())) {
                    continue;
                }

                $this->operatorHandlers[$operatorInterface->getOperator()]->addFilterToQueryBuilder($doctrineQueryBuilder, $dbPropertyAnnotation->getValues(), $operatorInterface);
            }
        }

        foreach ($filter->sortings as $sorting) {
            if (!in_array($sorting->getField(), $filterProperties)) {
                continue;
            }
            $reflectionProperty = new ReflectionProperty(get_class($filter), $sorting->getField());
            $sortDbPropertyAnnotation = $this->reader->getPropertyAnnotation($reflectionProperty, SortDbPropertyMapping::class);
            if ($sortDbPropertyAnnotation === null) {
                continue;
            }

            foreach ($sortDbPropertyAnnotation->getValues() as $sortFieldname) {
                $expr = null;
                switch ($sorting->getDirection()) {
                    case Ascending::DIRECTION:
                        $expr = $doctrineQueryBuilder->expr()->asc($sortFieldname);
                        break;
                    case Descending::DIRECTION:
                        $expr = $doctrineQueryBuilder->expr()->desc($sortFieldname);
                        break;
                }

                if ($expr) {
                    $doctrineQueryBuilder->addOrderBy($expr);
                }
            }
        }

        $doctrineQueryBuilder->setMaxResults($filter->limit);
        $doctrineQueryBuilder->setFirstResult($filter->offset);

        return $doctrineQueryBuilder;
    }
}
