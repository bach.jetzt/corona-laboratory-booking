<?php

namespace App\Filter;

use App\Filter\Annotations as Filter;
use App\Filter\Operators\Equals;
use App\Filter\Operators\GreaterThan;
use App\Filter\Operators\GreaterThanOrEquals;
use App\Filter\Operators\LessThan;
use App\Filter\Operators\LessThanOrEquals;
use App\Filter\Operators\Like;

class RoomFilter extends AbstractFilter
{
    /**
     * @Filter\DbPropertyMapping({"r.number", "r.floorName", "r.building"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"r.building", "r.floorName", "r.number"})
     */
    public ?array $search = null;

    /**
     * @Filter\DbPropertyMapping({"r.number"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"r.number"})
     */
    public ?array $number = null;
    /**
     * @Filter\DbPropertyMapping({"r.floorName"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"r.floorName"})
     */
    public ?array $floorName = null;
    /**
     * @Filter\DbPropertyMapping({ "r.building"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"r.building"})
     */
    public ?array $building = null;

    /**
     *
     * @Filter\DbPropertyMapping({"r.id"})
     * @Filter\AllowedOperators({Equals::OPERATOR})
     */
    public ?array $id = null;

    /**
     * @Filter\DbPropertyMapping({"t.name"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"t.name"})
     */
    public ?array $teamName = null;

    /**
     * @Filter\DbPropertyMapping({"r.allowedVisitors"})
     * @Filter\AllowedOperators({Equals::OPERATOR, LessThan::OPERATOR, LessThanOrEquals::OPERATOR, GreaterThan::OPERATOR, GreaterThanOrEquals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"r.allowedVisitors"})
     */
    public ?array $allowedVisitors = null;
}
