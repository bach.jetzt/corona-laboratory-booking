<?php

namespace App\Filter\Annotations;

/**
 * @Annotation
 */
class AllowedOperators
{
    /**
     * @var string[]
     */
    private array $values;

    public function __construct(array $allowedOperators)
    {
        $this->values = $allowedOperators['value'];
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
