<?php

namespace App\Filter\Annotations;

/**
 * @Annotation
 */
class DbPropertyMapping
{
    /**
     * @var string[]
     */
    private array $values;

    public function __construct(array $dbFields)
    {
        $this->values = $dbFields['value'];
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
