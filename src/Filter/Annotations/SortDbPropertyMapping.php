<?php

namespace App\Filter\Annotations;

/**
 * @Annotation
 */
class SortDbPropertyMapping
{
    /**
     * @var string[]
     */
    private array $values;

    public function __construct(array $sortFields)
    {
        $this->values = $sortFields['value'];
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
