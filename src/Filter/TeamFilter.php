<?php

namespace App\Filter;

use App\Filter\Annotations as Filter;
use App\Filter\Operators\Equals;
use App\Filter\Operators\Like;

class TeamFilter extends AbstractFilter
{
    /**
     * @Filter\DbPropertyMapping({"t.id"})
     * @Filter\AllowedOperators({ Equals::OPERATOR})
     */
    public ?array $id = null;

    /**
     * @Filter\DbPropertyMapping({"t.name"})
     * @Filter\AllowedOperators({Like::OPERATOR, Equals::OPERATOR})
     * @Filter\SortDbPropertyMapping({"t.name"})
     */
    public ?array $teamName = null;

    /**
     * @Filter\DbPropertyMapping({"t.invitationCode"})
     * @Filter\AllowedOperators({Equals::OPERATOR})
     */
    public ?array $invitationCode = null;
}
