<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Serializer\SerializerInterface;

class ValidationExceptionListener
{
    private SerializerInterface $serializer;

    /**
     * ValidationExceptionListener constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();
        if (!$exception instanceof RequestValidationException) {
            return;
        }

        /** @var RequestValidationException $exception */

        $errors = $this->serializer->serialize($exception->getViolationList(), 'json');
        $response = new Response();

        $response->setStatusCode(400);
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($errors);

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}
