<?php

namespace App\EventListener;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Persistence\Proxy as LegacyProxy;
use Doctrine\Persistence\Proxy;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function array_filter;
use function get_class;
use function is_array;

class ParameterValidationControllerListener implements EventSubscriberInterface
{
    private LoggerInterface $logger;
    private Reader $reader;
    private ValidatorInterface $validator;

    public function __construct(Reader $reader, LoggerInterface $logger, ValidatorInterface $validator)
    {
        $this->logger = $logger;
        $this->reader = $reader;
        $this->validator = $validator;
    }

    public function onKernelController(KernelEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller) && method_exists($controller, '__invoke')) {
            $controller = [$controller, '__invoke'];
        }

        if (!is_array($controller)) {
            return;
        }

        $className = $this->getRealClass(get_class($controller[0]));
        $object = new ReflectionClass($className);
        $method = $object->getMethod($controller[1]);

        $methodAnnotations = $this->reader->getMethodAnnotations($method);
        if (count($methodAnnotations) === 0) {
            return;
        }
        $myAnnotations = array_filter($methodAnnotations, fn($annotation) => get_class($annotation) === ParameterValidation::class);
        if (count($myAnnotations) == 0) {
            return;
        }

        $finalViolations = new ConstraintViolationList();

        foreach ($myAnnotations as $myAnnotation) {
            $property = $myAnnotation->getProperty();
            $data = $event->getRequest()->attributes->get($myAnnotation->getProperty());
            $violations = $this->validator->validate($data, $myAnnotation->getConstraints());
            /** @var ConstraintViolation $v */
            foreach ($violations as $v) {
                $finalViolations->add(
                    new ConstraintViolation(
                        $v->getMessage(),
                        $v->getMessageTemplate(),
                        $v->getParameters(),
                        $v->getRoot(),
                        $property,
                        $v->getInvalidValue(),
                        $v->getPlural(),
                        $v->getCode(),
                        $v->getConstraint(),
                        $v->getCause()
                    )
                );
            }
        }

        if ($finalViolations->count() > 0) {
            throw new RequestValidationException($finalViolations);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    private static function getRealClass(string $class): string
    {
        if (class_exists(Proxy::class)) {
            if (false === $pos = strrpos($class, '\\' . Proxy::MARKER . '\\')) {
                return $class;
            }

            return substr($class, $pos + Proxy::MARKER_LENGTH + 2);
        }

        if (class_exists(LegacyProxy::class)) {
            if (false === $pos = strrpos($class, '\\' . LegacyProxy::MARKER . '\\')) {
                return $class;
            }

            return substr($class, $pos + LegacyProxy::MARKER_LENGTH + 2);
        }

        return $class;
    }
}
