<?php

namespace App\EventListener;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ParameterValidation
{
    /**
     * @var Constraint[]
     */
    private $constraints;
    private $property;

    public function __construct($options)
    {
        $this->property = $options["property"];
        $this->constraints = $options["constraints"];
    }

    /**
     * @return Constraint[]
     */
    public function getConstraints(): array
    {
        return $this->constraints;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }
}
