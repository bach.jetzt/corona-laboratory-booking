<?php

namespace App\EventListener;

use App\Filter\AbstractPagedResult;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PagedResultKernelViewListener implements EventSubscriberInterface
{
    private NormalizerInterface $serializer;
    private AutoMapperInterface $mapper;

    public function __construct(NormalizerInterface $serializer, AutoMapperInterface $mapper)
    {
        $this->serializer = $serializer;
        $this->mapper = $mapper;
    }

    public function onKernelView(KernelEvent $event)
    {
        $controllerResult = $event->getControllerResult();
        if (is_subclass_of($controllerResult, AbstractPagedResult::class)) {
            $arrayPayload = $this->serializer->normalize($controllerResult->getData(), 'json');
            $response = new JsonResponse($arrayPayload, Response::HTTP_OK, [
                'X-Total-Count' => $controllerResult->totalCount,
                'X-Offset' => $controllerResult->offset,
                'X-Limit' => $controllerResult->limit,
                'X-Count' => $controllerResult->getCount()
            ]);
            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => 'onKernelView',
        ];
    }
}
