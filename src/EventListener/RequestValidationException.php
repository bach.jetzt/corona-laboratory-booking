<?php

namespace App\EventListener;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class RequestValidationException extends Exception
{
    private ConstraintViolationListInterface $violationList;

    public function __construct(ConstraintViolationListInterface $violationList, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->violationList = $violationList;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
