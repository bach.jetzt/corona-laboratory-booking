#!/usr/bin/env bash

#podman run -it -p 3307:3306 -e MYSQL_ROOT_PASSWORD=mysecretpw -v clb-db-data:/var/lib/mysql mariadb:10.5 mysqld  --character-set-server=utf8mb4 --collation-server=utf8mb4_swedish_ci
podman run -it -d --name clb-db -p 3307:3306 -e MYSQL_ROOT_PASSWORD=mysecretpw -v clb-db-data:/var/lib/mysql mysql:5.7.30  --character-set-server=utf8mb4 --collation-server=utf8mb4_swedish_ci

podman run -it -d --name clb-web -p 8000:8080 --security-opt label=disable -e PHP.xdebug.remote_host=i72 -e PHP_INI_SCAN_DIR=:/p/gd -e VHOST=symfony4 -e PHP.error_reporting=-1 -e PHP.display_errors=On -e PHPFPM_XDEBUG=On -v .:/var/www r0mm0n/php-apache:7.4
